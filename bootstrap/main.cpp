#include "main.h"
#include "include.h"
//#include "Amilai.h"
#include <ARKE/Application.h>
#include "lib/Registrar.h"

void InitApplication() {
    Application::BeginInitialization();
    Application::SetDataDir("../../");
    Application::SetName("Amilai2");
    Application::SetCodename("amilai.core");
    Application::SetCompanyName("Delta III Tech");
    Application::SetVersion("0.0r1X");
    Application::SetDebug(true);
    Application::EndInitialization();
}
//Initialize application structures prior to dl-trigged invocation of AI components
//_da_invoke(150, InitApplication);
_early_invoke(InitApplication);

int main(int argc, char** argv) {
    //_unused(argc); _unused(argv);
    try {
        int code = proxy_main(argc, argv);
        Application::Quit(code);
    } catch (Application::Exiting e) {
        if(e.code > 0) Console::Crit("Sig ",e.code,": ",e.what());
        return e.code;
    } catch (std::exception e) {
        Console::Crit("EXCEPTION: ",e.what());
        return 1;
    }
}