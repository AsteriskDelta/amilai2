#include "RelationInstance.h"
#include "Relation.h"

RelationInstance::RelationInstance() : relation(nullptr), members(), significance(pfloat(0.0)) {

}

RelationInstance::RelationInstance(RelationPtr rel, const std::vector<SymbolNode*>& mem, const pfloat& sig) : relation(rel), members(mem), significance(sig) {

}

RelationInstance::RelationInstance(const RelationInstance& orig)  : relation(orig.relation), members(orig.members), significance(orig.significance){

}

RelationInstance::~RelationInstance() {

}

bool RelationInstance::operator==(const RelationInstance& o) const {
    return relation == o.relation && significance == o.significance && members == o.members;
}

bool RelationInstance::isSignificant() const {
    return this->significance > pfloat(0.0);
}

std::string RelationInstance::toString() const {
    std::stringstream ss;
    if(relation != nullptr) {
        ss << "RelI(" << relation->idText() <<", "<<significance<<", ["<<members.size()<<"{";
        for(auto it = members.begin(); it != members.end(); ++it) {
            if(it != members.begin()) ss << ", ";
            ss << *it;
        }
        ss << "} )";
    } else ss << "RelI(nullptr)";
    return ss.str();
}

void RelationInstance::instanceCopy(RelationInstance *const o) const {
    o->members = this->members;
}