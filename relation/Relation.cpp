#include "Relation.h"
#include "symbol/SymbolExt.h"
#include "ai/AIInstance.h"
#include "stream/Stream.h"

Relation::Relation() {
}

Relation::~Relation() {
}

std::string Relation::idText() const {
    std::stringstream ss;
    idint printSymID = this->id();
    const bool isGlobalSym = printSymID > (idint(1)<<61);
    if(isGlobalSym) {
        printSymID = (idint(1)<<62) - printSymID;
        ss << "g_";
    }
    ss << printSymID;
    return ss.str();
}

RelationInstance Relation::evaluate(std::vector<SymbolNode*> &evArr, unsigned int nodeOffset, TailCallData *const tcData) {
    _unused(tcData);
    if(!(*this)) {
        Console::Warn("Relation ",this->idText()," of order -1 was requested an evaluation!");
        return RelationInstance(this, evArr, pfloat(0.0));
    }
    if(evArr.size() < this->order()) {
        Console::Warn("Relation ",this->idText()," of degree ",this->degree(),", order",this->order()," requested to evaluate ",nodes.size()," nodes!");
        return RelationInstance(this, evArr, pfloat(0.0));
    }
    
    pfloat sig = pfloat(0.0), sigDiv = pfloat(0.0), threshold = pfloat(0.001); _unused(threshold);
    if(nodeOffset > this->degree() || nodeOffset < this->order()) nodeOffset = this->order();//Skip "known" by default, and always skip givens
    //std::cout << "\trel evaluate from offset=" << nodeOffset << "\n";
    //Evaluate every node that we have information about, skipping those we've already scored
    for(unsigned int i = nodeOffset; i < this->degree(); i++) {
        NodeData *const nodeData = &nodes[i];
        pfloat nodeScore = pfloat(0.0);
        const pfloat nodeWeight = nodeData->contribution;
        
        SymbolNode *symNode = nullptr;
        
        if(i < evArr.size() && evArr[i] != nullptr) {//Evaluate existing node
            symNode = evArr[i];
        } else {//Discover new node
            SymbolVector searchPos, searchRange;
            nodeData->estimate(&evArr, searchPos, searchRange);//pos and range by reference
            symNode = _stream->nearest(searchPos, searchRange);//Get best match from stream
        }
        
        if(symNode == nullptr) {
            sig = pfloat(0.0); break;
        }
        nodeScore = nodeData->evaluate(symNode, &evArr);
        
        if(evArr.size() <= i) evArr.resize(i+1);
        if(evArr[i] != symNode) evArr[i] = symNode;
        
        sig += nodeScore;
        sigDiv += nodeWeight;
        //std::cout << "\tsc " << sig << " / " << sigDiv << "\n";
        
        //Score, and give up if the best match is too unlikely to matter
        const pfloat sigTotal = this->totalNodeWeight();
        const pfloat sigRatio = sigDiv/sigTotal;
        if(sig/sigDiv * (sigRatio)  + (1.0 - sigRatio) < threshold) {
            sig = pfloat(0.0); sigDiv = pfloat(1.0);
            break;
        }
    }
    
    sig = clamp(sig / sigDiv, pfloat(0.0), pfloat(this->totalNodeWeight()));
    
    return RelationInstance(this, evArr, sig);
}

Relation::OrderData::OrderData() : mult(), principle() {
    
}
Relation::NodeData::NodeData() : offset(), curve(pfloat(1.0)), contribution(pfloat(1.0)), order() {
    
}

void Relation::NodeData::estimate(std::vector<SymbolNode*> *const ref, SymbolVector &pos, SymbolVector &principle) {
    pos = offset; principle = eigen;
    
    //Calculate the position and principle (range) as a function of $order given points (via ref)
    for(unsigned int i = 0; i < order.size(); i++) {
        OrderData *const orderData = &order[i];
        pos += ref->at(i)->symv() * orderData->mult;
        principle += ref->at(i)->symv() * orderData->principle;
        //principle.clampPositive();//Ensure our principle vectors are non-negative
    }
}

pfloat Relation::NodeData::evaluate(SymbolNode *const node, std::vector<SymbolNode*> *const ref) {
    SymbolVector pos, principle;
    this->estimate(ref, pos, principle);
    
    return pos.scoreTo(node->symv(), principle, curve);
    /*
    const SymbolVector delta = pos.deltaTo(node->symv());
    SymbolVector ratios = delta / principle;
    //ratios approach "legs" of N-dimensional hypercube as delta->principle
     curve
    if(ratios.maxMagnitude() > pfloat(1.0)) return pfloat(0.0);//If we exceed any bounds, the test fails
    
    const pfloat maximumDistance = sqrt(pfloat(delta.size()));
    const pfloat actualDistance = ratios.magnitude();
    //Thus, actualDistance -> maximumDistance as delta->principle
    
    //And aD/mD -> 1 as delta->principle, and aD/mD -> 0 as delta->null, so take the reciprocal
    return pfloat(1.0) - pow(actualDistance / maximumDistance, curve);*/
}

pfloat Relation::totalNodeWeight() {
    pfloat ret = pfloat(0.0);
    for(NodeData& n : nodes) ret += n.contribution;
    return ret;
}



//For inline reference, obsolete algorithms for scoring a relation
/*
    pfloat sig = pfloat(1.0);
    auto currentNode = nodes.begin();
    OrderData actual = orders.front();
    SymbolVector prevDelta = SymbolVector();
    
    for(unsigned int i = 0; i < orders.size(); i++) {
        auto nextNode = std::next(currentNode);
        OrderData *const order = &orders[i];
        
        //Zero-order is invariant
        if(i == 1) {//1st order
            actual.vector += order->vector;
            actual.principle = order->principle;
            actual.coeff = order->coeff;
        } else if(i > 1) {//nth order
            SymbolVector localDelta = (*currentNode)->symv().deltaTo((*nextNode)->symv());
            SymbolVector delta = prevDelta.deltaTo(localDelta);

            actual.vector += order->vector * delta;
            actual.principle = order->principle;// * delta;
            actual.coeff = order->coeff;// * delta.magnitude();

            prevDelta = delta;
            currentNode = nextNode;
        }
        
        pfloat actSig = actual->evaluate(*currentNode);
        sig *= actSig;
        if(sig <= pfloat(0.0)) break;
        
        if(i == 0) {//Update rel
            actual.vector = (*currentNode)->symv();
        }
    }*/
    //Calculate the center
    /*
    OrderData actual = orders[0]; auto currentNode = nodes.begin();
    SymbolVector prevDelta = SymbolVector();
    
    actual.vector = (*currentNode)->symv();
    
    for(unsigned int i = 1; i < orders.size(); i++) {
        auto nextNode = std::next(currentNode);
        
        OrderData *const order = &orders[i];
        if(i == 1) {//1st order- constant multiple, delta is the identity vector
            actual.vector += order->vector;
            actual.principle += order->principle;
            actual.coeff += order->coeff;
        } else {//Nth order- function of d/dx
            SymbolVector localDelta = (*currentNode)->symv().deltaTo((*nextNode)->symv());
            SymbolVector delta = prevDelta.deltaTo(localDelta);

            actual.vector += order->vector * delta;
            actual.principle += order->principle * delta;
            actual.coeff += order->coeff * delta.magnitude();

            prevDelta = delta;
            currentNode = nextNode;
        }
    }*/
    
    //Evaluate final node's score
    //sig = actual->evaluate(nodes.back());*/
