#include "Relation.h"

#include "RelationVector.h"
#include "RelationInstance.h"
#include "lib/InstanceVector.cpp"

template class InstanceVector<RelationInstance, RelationPtr>;
