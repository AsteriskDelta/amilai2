#ifndef SYMBOLWAVEFN_H
#define SYMBOLWAVEFN_H
#include "include.h"
#include "symbol/SymbolPtr.h"
#include "symbol/SymbolInstance.h"
#include "symbol/SymbolVector.h"
#include <list>
#include <vector>
#include "RelationInstance.h"

class SymbolNode;

class Relation {
public:
    /*struct OrderData {
        SymbolVector vector;//ie: 0center, 1offest, 2multiplicative, etc.
        SymbolVector principle;//Range principle vectors, as above;
        pfloat coeff;//Coeffs of principle vectors
        
        OrderData();//Initialize with identity components
        pfloat evaluate(SymbolNode *const node) const;
        
    };*/
    struct OrderData {
        SymbolVector mult;
        SymbolVector principle;
        OrderData();
    };
    struct NodeData {
        SymbolVector offset, eigen;
        
        //curves>1 approach 0-score slower at first, and vice versa
        //Contribution weight for node, relative to all other weights
        pfloat curve, contribution;
        
        std::vector<OrderData> order;
        
        NodeData();
        inline OrderData& operator[](const unsigned int i) { return order[i]; };
        
        void estimate(std::vector<SymbolNode*> *const ref, SymbolVector &pos, SymbolVector &principle);
        pfloat evaluate(SymbolNode *const node, std::vector<SymbolNode*> *const ref);
    };
    
    struct TailCallData {
        std::vector<pfloat> searchMin;
    };
    
    Relation();
    virtual ~Relation();
    
    idint relID;
    
    std::list<Relation*> parents, children;
    std::list<Relation*> rels;//Of $order elements
    
    unsigned short llOrder;
    std::vector<NodeData> nodes;
    
    pfloat significance;//Significance of relation
    
    inline idint id() const { return relID; };
    inline unsigned int order() const { return llOrder; };
    inline unsigned int degree() const { return nodes.size(); };
    inline operator bool() const { return this->degree() > this->order(); };
    
    pfloat totalNodeWeight();
    
    //Provides tail-call option to access sub-optimal solutions (and therefore avoid local maxima)
    RelationInstance evaluate(std::vector<SymbolNode*> &evArr, unsigned int nodeOffset = (1<<31), TailCallData *const tcData = nullptr);
    
    std::string idText() const;
    
    inline bool hasName() const { return false; };
    inline std::string getName() const { return ""; };
private:
    unsigned long long refCount;
    unsigned int indexCount;
public:
    inline void incRef() { refCount++; };
    inline void decRef() { refCount--; };
    inline void incIdx() { indexCount++; };
    inline void decIdx() { indexCount--; };
};

#endif /* SYMBOLWAVEFN_H */

