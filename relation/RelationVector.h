#ifndef RELATIONVECTOR_H
#define RELATIONVECTOR_H

#include "include.h"
#include "RelationPtr.h"
#include "RelationInstance.h"
#include "lib/InstanceVector.h"

typedef InstanceVector<RelationInstance, RelationPtr> RelationVector;


#endif /* RELATIONVECTOR_H */

