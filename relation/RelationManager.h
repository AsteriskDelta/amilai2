#ifndef RELATIONMANAGER_H
#define RELATIONMANAGER_H
#include "include.h"
#include "RelationPtr.h"
#include "Relation.h"
#include <lib/InstanceManager.h>

struct RelationLoadData {
    idint id;
};

class RelationManager : public InstanceManager<idint, RelationPtr, RelationLoadData> {
public:
    typedef InstanceManager<idint, RelationPtr, RelationLoadData> InstanceManagerParent;
    
    RelationManager(bool isGlobal = false);
    RelationManager(const RelationManager& orig) = delete;
    virtual ~RelationManager();
    
    RelationPtr load(idint id);
    void unload(idint id);
protected:
    
public:
    
};

#endif /* RELATIONMANAGER_H */

