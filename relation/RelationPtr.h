#ifndef RELATIONPTR_H
#define RELATIONPTR_H
#include "include.h"
#include "lib/DeferredPtr.h"
#include "RelationResolve.h"

class Relation;
typedef DeferredPtr<Relation, idint, RelationResolve, IDINT_NONE> RelationPtr;

#endif /* RELATIONPTR_H */

