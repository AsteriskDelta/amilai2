#include "RelationManager.h"
#include "lib/InstanceManager.cpp"
#include "lib/Registrar.h"
//#include <algorithm>

__thread RelationManager *_relations = nullptr;
RelationManager *_globalRelations;
template class InstanceManager<idint, RelationPtr, RelationLoadData>;

static void AllocRelationManager() {
    _globalRelations = new RelationManager(true);
    Console::Out("Global RelationManager allocated at ", _globalRelations);
}
_preregister(AllocRelationManager);

RelationManager::RelationManager(bool isGlobal) : InstanceManagerParent(isGlobal) {
}

RelationManager::~RelationManager() {
}

RelationPtr RelationManager::load(idint id) {
    _unused(id);
    return RelationPtr(nullptr);
}
void RelationManager::unload(idint id) {
    _unused(id);
}