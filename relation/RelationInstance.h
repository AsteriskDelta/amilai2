#ifndef RELATIONINSTANCE_H
#define RELATIONINSTANCE_H
#include "include.h"
#include "RelationPtr.h"
#include <list>
#include <vector>

class SymbolNode;

class RelationInstance {
public:
    RelationInstance();
    RelationInstance(RelationPtr rel, const std::vector<SymbolNode*>& mem, const pfloat& sig = pfloat(0.0));
    inline RelationInstance(RelationPtr rel, const pfloat& sig, const pfloat& unu) : relation(rel), significance(sig) { _unused(unu); };
    RelationInstance(const RelationInstance& orig);
    virtual ~RelationInstance();
    
    bool operator==(const RelationInstance& o) const;
    inline bool operator !=(const RelationInstance& o) const { return !(*this == o); };
    
    bool isSignificant() const;
    inline operator bool() const {return this->isSignificant(); };
    
    RelationPtr relation;
    std::vector<SymbolNode*> members;
    
    pfloat significance;
    
    inline RelationPtr rel() const { return relation; };
    inline pfloat sig() const { return significance; };
    inline pfloat magnitude() const { return this->sig(); };
    inline RelationPtr instance() const { return this->rel(); };
    
    inline pfloat val() const { return significance; };
    inline pfloat nval() const { return -(this->val()); };
    inline pfloat probability() const { return 1.0; };
    
    std::string toString() const;
    
    void instanceCopy(RelationInstance *const o) const;
private:

};

#endif /* RELATIONINSTANCE_H */

