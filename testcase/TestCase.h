#ifndef TESTCASE_H
#define TESTCASE_H
#include "lib/Registrar.h"
#include "Amilai.h"
#include <iostream>

class TestCase : public NamedRegistrar<TestCase> {
public:
    struct Result {
        struct {
            int prepare, train, evaluate;
        } status;

        pfloat score;
        friend std::ostream& operator<< (std::ostream& stream, const Result& r) {
            stream << "TestCase::Result @ " << r.score << " status{"<<r.status.prepare<<","<<r.status.train<<","<<r.status.evaluate<<"}";
            return stream;
        };
    };
    
    TestCase(const std::string& n);
    virtual ~TestCase();
    
    virtual int prepare();
    virtual int train();
    
    virtual pfloat evaluate();
    
    virtual Result execute();
    
    inline const std::string& getName() const {
        return name;
    }
protected:
    static void TryRegistrarSetup();
    
    std::string name;
};

#endif /* TESTCASE_H */

