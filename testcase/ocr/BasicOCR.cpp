#include "BasicOCR.h"

#include <ARKE/AppData.h>
#include <ARKE/Image.h>
#include <ARKE/ImageFont.h>

namespace AITest {
    
struct OCRTestImgDat {
    unsigned int width, height, pt, x, y;
    std::string text;
};

BasicOCR::BasicOCR() : TestCase(Name()) {
    
}

BasicOCR::~BasicOCR() {
}

static bool mkTestText(const std::string& path, ImageFont *font, OCRTestImgDat& dat) {
    ImageRGBA *img = new ImageRGBA(dat.width, dat.height);
    unsigned int allocSize = dat.width*dat.height;
    
    ColorRGBA background = ColorRGBA(255,255,255,255);
    for(unsigned int i = 0; i < allocSize; i++) img->data[i] = background;
    
    font->write(img, dat.x, dat.y, dat.text);
    bool ret = img->Save(path);
    //std::cout << "Writing img to " << path << ", ret = " << ret << "\n";
    delete img;
    return ret;
}

int BasicOCR::prepare() {
    const std::string relRoot = "testcase/ocr/";
    const std::string metaFilePath = relRoot+"cases/.gen";
    
    if(AppData::base.exists(metaFilePath) && AppData::base.exists(relRoot+"cases/a0.png")) {
        Console::Out("BasicOCR using existing corpus at ",relRoot,"cases/ (delete ", metaFilePath, " to regenerate it)");
        return 2;
    }
    
    Console::Out("BasicOCR preparing letter corpus to ",relRoot,"cases/");
    ImageFont::Initialize();
    //srand(50);
    
    for(unsigned int fontID = 0; fontID <= 5; fontID++) {
      const std::string fontFile = relRoot+std::to_string(fontID)+".ttf";
      ImageFont *font = new ImageFont(fontFile, 30);
      #pragma omp parallel for 
      for(char c = 'a'; c <= 'z'; c++) {
        const std::string outFile = relRoot+"cases/"+std::string(1, c)+std::to_string(fontID)+".png";
        OCRTestImgDat img;
        img.text = std::string(1, c);// unsigned int cl = img.text.size();
        img.width  = 70;// + (rand() % 20 * 4);
        img.height = 70;// + (rand() % 20 * 4);
        img.pt = 30;//std::min(img.width, img.height)/(8* (cl/2 + 1));
        img.x = 10;//rand() % 15,
        img.y = 10;//rand() % 15;
        mkTestText(outFile, font, img);
      }
      
      delete font;
    }
    
    AppData::base.touch(metaFilePath);
    Console::Out("\tCorpus written OK...");
    
    return 1;
}
int BasicOCR::train() {
    
    return 0;
}
pfloat BasicOCR::evaluate() {
    return 0.0;
}

_register(TestCase, BasicOCR);

};