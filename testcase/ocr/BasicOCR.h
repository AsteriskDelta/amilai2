#ifndef OCR_H
#define OCR_H
#include "Amilai.h"
#include "testcase/TestCase.h"

namespace AITest {

class BasicOCR : public TestCase {
public:
    BasicOCR();
    virtual ~BasicOCR();
    
    virtual int prepare() override;
    virtual int train() override;
    
    virtual pfloat evaluate() override;
    
    inline static std::string Name() { return "BasicOCR"; };
private:

};

};

#endif /* OCR_H */

