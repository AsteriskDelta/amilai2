#include "TestCase.h"

template class Registrar<TestCase>;

TestCase::TestCase(const std::string& n) : name(n) {
    
}

TestCase::~TestCase() {
}

int TestCase::prepare() {
    
    return 0;
}
int TestCase::train() {
    
    return 0;
}
pfloat TestCase::evaluate() {
    return 0.0;
}

TestCase::Result TestCase::execute() {
    Result result;
    result.status.prepare = this->prepare();
    result.status.train = this->train();
    result.score = this->evaluate();
    
    result.status.evaluate = 1;
    return result;
}

void TestCase::TryRegistrarSetup() {
    
}