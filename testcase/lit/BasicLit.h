#ifndef BASICLIT_H
#define BASICLIT_H
#include "Amilai.h"
#include "testcase/TestCase.h"

namespace AITest {

class BasicLit : public TestCase {
public:
    BasicLit();
    virtual ~BasicLit();
    
    virtual int prepare() override;
    virtual int train() override;
    
    virtual pfloat evaluate() override;
    
    inline static std::string Name() { return "BasicLit"; };
private:

};

};

#endif /* BASICLIT_H */

