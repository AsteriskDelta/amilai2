#ifndef STREAMSUBSET_H
#define STREAMSUBSET_H

#include "include.h"
#include "lib/hdi/HDISubset.h"
#include "symbol/SymbolPtr.h"
#include "symbol/SymbolNode.h"

#include "Stream.h"

typedef typename Stream::Subset StreamSubset;

#endif /* STREAMSUBSET_H */

