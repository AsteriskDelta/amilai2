#ifndef STREAMRANGE_H
#define STREAMRANGE_H

#include "include.h"
#include "lib/hdi/HDIRange.h"
#include "symbol/SymbolPtr.h"
#include "symbol/SymbolNode.h"

#include "Stream.h"

typedef typename Stream::Range StreamRange;

#endif /* STREAMRANGE_H */

