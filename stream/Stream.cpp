#include "Stream.h"

#include "lib/hdi/HDIndex.cpp"
#include "ai/AIInstance.h"

#include <vector>

thread_local Stream *_stream = nullptr;
template class HDIndex STREAM_SPC;

Stream::Stream() : Stream::HDI() {
    
}

Stream::~Stream() {
    
}

bool Stream::process() {
    SymbolNode* node;
    while((node = this->getUnprocessed()) != nullptr) {
        node->relate();
    }
}

/*

Stream::Stream() : root(nullptr) {
    this->makeRoot();
}

Stream::~Stream() {
    this->freeRoot();
}

void Stream::addNode(SymbolNode * const &node) {
    if (node == nullptr || root == nullptr) return;
    
    if(node->size() == 0) { DBG(Console::Warn("Stream::addNode rejected node with size=0, at ",node)); return; };
    if(node->isStreamed()) { DBG(Console::Warn("Stream::addNode already streamed node, at ",node)); return; };
    //rawNodes.emplace(node);
    root->emplace(node);
    node->setStreamed();
}

void Stream::removeNode(SymbolNode * const &node) {
    if (node == nullptr || root == nullptr) return;
    if(node->size() == 0) { DBG(Console::Warn("Stream::removeNode rejected node with size=0, at ",node)); return; };
    if(!node->isStreamed()) { DBG(Console::Warn("Stream::removeNode not yet streamed node, at ",node)); return; };
    
    root->remove(node);
    node->clearStreamed();
    //auto it = rawNodes.find(node);
  
    //if(it != rawNodes.end()) {
    //  rawNodes.erase(node);
    //}
}

void Stream::makeRoot() {
    root = new StreamPage(nullptr, SymbolPtr(nullptr));
}

void Stream::freeRoot() {
    if (root == nullptr) return;

    delete root;
}

void Stream::debugPrint(unsigned short lvl) {
  if(root != nullptr) root->debugPrint(lvl);
}*/