#ifndef STREAM_H
#define STREAM_H
#include "include.h"
#include "lib/hdi/HDIndex.h"
#include "symbol/SymbolPtr.h"
#include "symbol/SymbolVector.h"
#include "symbol/SymbolNode.h"

#define STREAM_SPC <idint, SymbolPtr, SymbolNode, pfloat, SymbolVector>

class Stream : public HDIndex STREAM_SPC {
public:
    typedef HDIndex STREAM_SPC HDI;
    
    Stream();
    virtual ~Stream();
    
    bool process();
protected:

};

#include "StreamPage.h"
#include "StreamRange.h"
#include "StreamSubset.h"
/*
#include "include.h"
#include "symbol/SymbolExt.h"
#include <unordered_set>
#include <unordered_map>

#include "StreamExt.h"

class Stream {
public:
    Stream();
    virtual ~Stream();
    
    void addNode(SymbolNode *const &node);
    void removeNode(SymbolNode *const &node);
    
    StreamPage *root;
    
    //StreamRow* getRow(SymbolPtr sym);
    
    void debugPrint(unsigned short lvl = 0);
protected:
    void makeRoot();
    void freeRoot();
   // StreamRow *addRow(SymbolPtr sym);
    //StreamRow *deleteRow(SymbolPtr sym);
    
    //std::unordered_set<SymbolNode*> rawNodes;
    //std::unordered_map<idint, StreamRow*> rawRows;
};
*/
#endif /* STREAM_H */

