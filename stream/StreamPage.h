#ifndef STREAMPAGE_H
#define STREAMPAGE_H

#include "include.h"
#include "lib/hdi/HDIPage.h"
#include "symbol/SymbolPtr.h"
#include "symbol/SymbolNode.h"

#include "Stream.h"

typedef typename Stream::Page StreamPage;

#endif /* STREAMPAGE_H */

