#ifndef INTRINSIC_H
#define INTRINSIC_H
#include "include.h"
#include "lib/Registrar.h"
#include "lib/TypeName.h"
#include "symbol/SymbolExt.h"
#include "stream/StreamExt.h"
#include <functional>
#include <unordered_map>

#include <iostream>

typedef std::function<SymbolPtr(const std::string& fullName, const std::string& symName)> MakeSymFunc;
using std::placeholders::_1; using std::placeholders::_2; using std::placeholders::_3;
#define intrinBind(f) std::bind(&f, this, _1, _2 )

class Intrinsic : public NamedRegistrar<Intrinsic> {
public:
    Intrinsic();
    Intrinsic(const std::string& nm);
    virtual ~Intrinsic();
    
    std::string name;
    
    virtual int initialize();
    virtual int terminate();
    
    virtual void wasInitialized();
    virtual void willTerminate();
    
    /*template<typename FT> inline MakeSymFunc bindSymFunc(FT f) {
        return std::bind(f, this, _1, _2 );
    }
    template<typename FT> inline MakeSymFunc bsf(const FT& f) { return bindSymFunc(f); };*/
    
    virtual SymbolPtr allocateSymbol(const std::string& symName, idint newID = IDINT_NONE);
    virtual SymbolPtr requireSymbol(const std::string& symName, MakeSymFunc mkFunc = MakeSymFunc());
    
    inline const std::string& getName() const {
        return this->name;
    }
    
    /*
    static std::unordered_map<std::string, Intrinsic*> registeredMap;
    static Intrinsic* ByName(const std::string& n);
    template<typename T>
    static inline T* ByName(const std::string& n) {
        return (T*)ByName(n);
    }
    template<typename T>
    static inline T* ByType() {
        const std::string name = T::Name();//_typeName<T>();
        std::cout << "tname: \"" << name << "\"\n";
        return ByName<T>(name);
    }
    
    static void MapIntrinsic(Intrinsic *const & intrin);
    static void UnmapIntrinsic(Intrinsic *const & intrin);
    
    static inline const std::string Name() {
        return "UNDEFINED";
    }*/
protected:
    static void TryRegistrarSetup();
};

#endif /* INTRINSIC_H */

