#ifndef TIME_H
#define TIME_H
#include "include.h"
#include "intrinsic/Intrinsic.h"

namespace Intrin {

class Time : public Intrinsic{
public:
    Time();
    virtual ~Time();
    
    int initialize() override;
    int terminate() override;
    
    SymbolPtr mkChanTime(const std::string& fullName, const std::string& n);
    
    inline SymbolPtr timeSym() {
        return this->sym.time;
    }
    
    static inline const std::string Name() {return "Time";};
private:
    struct {
        SymbolPtr time;
    } sym;
};

}
#endif /* TIME_H */

