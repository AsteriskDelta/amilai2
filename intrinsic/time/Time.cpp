#include "Time.h"

namespace Intrin {
  
Time::Time() : Intrinsic(Name()) {
  this->initialize();
}

Time::~Time() {
}

int Time::initialize() {
  const auto timeAxisFunc = intrinBind(mkChanTime);// bindSymFunc(&Time::mkChanTime);
  sym.time = requireSymbol("Time", timeAxisFunc);
  return 0;
}
int Time::terminate() {
  
  return 0;
}

SymbolPtr Time::mkChanTime(const std::string& fullName, const std::string& n) {
  SymbolPtr ret = this->allocateSymbol(fullName);
  _unused(n);
  
  return ret;
}

_register_da(Intrinsic, Time, 999);

};