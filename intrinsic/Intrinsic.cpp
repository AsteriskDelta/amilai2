#include "Intrinsic.h"

//REGISTRY_VARS(Intrinsic);
template class NamedRegistrar<Intrinsic>;

 void Intrinsic::TryRegistrarSetup() {
    //std::cout << "ALLOC INTRIN REGIS\n";
    if(!NamedRegistrar<Intrinsic>::HasRegisterCallbacks()) {
        NamedRegistrar<Intrinsic>::AddRegisterCallback(RegistryAlteredCB(Intrinsic::wasInitialized));
        NamedRegistrar<Intrinsic>::AddUnregisterCallback(RegistryAlteredCB(Intrinsic::willTerminate));
    }
}

Intrinsic::Intrinsic() {
    TryRegistrarSetup();
    
}

Intrinsic::Intrinsic(const std::string& nm) : name(nm) {
    TryRegistrarSetup();
    
}

Intrinsic::~Intrinsic() {
}

int Intrinsic::initialize() {
    return 0;
}
int Intrinsic::terminate() {
    return 0;
}
void Intrinsic::wasInitialized() {
    Console::Out("Intrinsic module '", this->getName(), "' was injected into runtime at ", this);
}
void Intrinsic::willTerminate() {
    Console::Out("Intrinsic module '", this->getName(), "' at ", this, " will be terminated...");
}


SymbolPtr Intrinsic::allocateSymbol(const std::string& symName, idint newID) {
    if(newID == IDINT_NONE) newID = _globalSymbols->getNewID();
    return new Symbol(symName, newID);
}
    
SymbolPtr Intrinsic::requireSymbol(const std::string& symName, MakeSymFunc mkFunc) {
    std::string fullName = name + "." + symName;
    SymbolPtr ret = _globalSymbols->get(fullName);
    if(!ret) {
        if(mkFunc) ret = mkFunc(fullName, symName);
        else {
            ret = this->allocateSymbol(fullName);
        }
        _globalSymbols->add(ret);
    }
    
    return ret;
}
/*
std::unordered_map<std::string, Intrinsic*> _dalloc_pre() Intrinsic::registeredMap;
Intrinsic* Intrinsic::ByName(const std::string& n) {
    auto it = registeredMap.find(n);
    if(it == registeredMap.end()) return nullptr;
    else return it->second;
}

void Intrinsic::MapIntrinsic(Intrinsic *const & intrin) {
    std::cout << "mapin " << intrin->getName() << "\n";
    registeredMap[intrin->getName()] = intrin;
}
void Intrinsic::UnmapIntrinsic(Intrinsic *const & intrin) {
    auto it = registeredMap.find(intrin->getName());
    
    if(it != registeredMap.end()) registeredMap.erase(it);
}*/