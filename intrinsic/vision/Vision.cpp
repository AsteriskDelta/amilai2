#include "Vision.h"
#include <ARKE/Image.h>

namespace Intrin {

Vision* activeVision = nullptr;
  
Vision::Vision() : Intrinsic(Name()) {
  luminanceOnly = true;
  activeVision = this;
  this->initialize();
}

Vision::~Vision() {
}

int Vision::initialize() {
    const auto axisFunc = intrinBind(mkAxisSym);
    sym.x = requireSymbol("RawX", axisFunc);
    sym.y = requireSymbol("RawY", axisFunc);
    //Crosslink X and Y for invariant transformations
    
    const auto channelFunc = intrinBind(mkChanSym);
    if(luminanceOnly) {
      sym.bw = requireSymbol("Luminance", channelFunc);
    } else {
      sym.r = requireSymbol("Red", channelFunc);
      sym.g = requireSymbol("Green", channelFunc);
      sym.b = requireSymbol("Blue", channelFunc);
      
      //Crosslink channels?
    }
    
    return 0;
}

int Vision::terminate() {
    return 0;
}

SymbolNode* Vision::mkPixelNode(pfloat x, pfloat y, pfloat bw) {
  SymbolNode *ret = new SymbolNode();
  ret->addInstance(SymbolInstance(sym.x, x));
  ret->addInstance(SymbolInstance(sym.y, y));
  ret->addInstance(SymbolInstance(sym.bw, bw));
  
  return ret;
}

void Vision::presentImage(const std::string& path) {
   Console::Out("Vision: loading image at ",path);
   ImageRGBA *img = new ImageRGBA(path); unsigned int nodeCount = 0;
   
   if(*img) {
     for(unsigned int x = 0; x < img->width; x++) {
       for(unsigned int y = 0; y < img->height; y++) {
         const ColorRGBA col = img->getPixel(x, y);
         SymbolNode *pixNode = this->mkPixelNode(pfloat(x), pfloat(y), pfloat(col.flt(0)));
         
         _stream->add(pixNode);
         nodeCount++;
       }
     }
     
     Console::Out("\t\tpresented ",img->width,"x",img->height," image to stream, b/w over ", nodeCount, " symnodes");
   }
   
   delete img;
}

SymbolPtr Vision::mkAxisSym(const std::string &fullName, const std::string& n) {
    SymbolPtr ret = this->allocateSymbol(fullName);
    _unused(n);
    
    return ret;
}
SymbolPtr Vision::mkChanSym(const std::string &fullName, const std::string& n) {
    SymbolPtr ret = this->allocateSymbol(fullName);
    _unused(n);
    
    return ret;
}

_register(Intrinsic, Vision);

};