#ifndef VISION_H
#define VISION_H
#include "include.h"
#include "intrinsic/Intrinsic.h"

namespace Intrin {

class Vision : public Intrinsic {
public:
    Vision();
    virtual ~Vision();
    
    int initialize() override;
    int terminate() override;
    
    SymbolNode* mkPixelNode(pfloat x, pfloat y, pfloat bw);
    
    void presentImage(const std::string& path);
    
    SymbolPtr mkAxisSym(const std::string &fullName, const std::string& n);
    SymbolPtr mkChanSym(const std::string& fullName, const std::string& n);
    
    static inline const std::string Name() {return "Vision";};
private:
    struct {
        SymbolPtr x, y;
        SymbolPtr r, g, b;
        SymbolPtr bw;
    } sym;
    
    bool luminanceOnly;
};


extern Vision *activeVision;
};

#endif /* VISION_H */

