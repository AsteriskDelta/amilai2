#include "TTY.h"
#include <ARKE/AppData.h>
#include "intrinsic/time/Time.h"
#include <cctype>
#include "symbol/SymbolNode.h"

namespace Intrin {

TTY::TTY() : Intrinsic(Name()) {
  this->initialize();
}

TTY::~TTY() {
}

int TTY::initialize() {
  sym.alphas.resize(128);
  addAlphaSym(8);
  addAlphaSym(9);
  addAlphaSym(10);
  addAlphaSym(13);
  addAlphaSym(27);
  for(char c = 32; c <= 126; c++) {
    addAlphaSym(c);
  }
  
  return 0;
}
int TTY::terminate() {
  
  return 0;
}

static SymbolNode* mkCharNode(SymbolPtr &charSym, double t, SymbolPtr timeSym) {
    SymbolNode *ret = new SymbolNode();
    ret->addInstance(SymbolInstance(charSym, 0.0));
    ret->addInstance(SymbolInstance(timeSym, t));
    return ret;
}

void TTY::presentText(const std::string& file, float dT) {
    if(!AppData::base.exists(file)) {
        Console::Err("TTY::presentText '",file,"' does not exist!");
        return;
    }
    
    Console::Out("TTY: presenting contents of '",file,"' with virtual dT=",dT);
    double time = 0.0;
    unsigned int fLength, syms = 0;;
    char* fData = AppData::base.readFileText(file, fLength);
    SymbolPtr timeSym = Intrinsic::ByType<Intrin::Time>()->timeSym();
    
    std::stringstream rawOut;
    for(unsigned int i = 0; i < std::min((unsigned int)1000,fLength); i++) {
        char c = std::tolower(fData[i]);
        SymbolPtr charSym = this->getAlphaSym(c);
        if(charSym == nullptr) continue;
        
        SymbolNode *cNode = mkCharNode(charSym, time, timeSym);
        _stream->add(cNode);
        
        rawOut << c;
        time += double(dT); syms++;
    }
    
    Console::Out("Contents: ");
    Console::Out(rawOut.str());
    Console::Out("\t\tpresented ",syms," characters over ",time, " virtual seconds");
    
    delete[] fData;
}

SymbolPtr TTY::mkChanAlpha(const std::string& fullName, const std::string& n) {
  SymbolPtr ret = this->allocateSymbol(fullName);
  _unused(n);
  
  return ret;
}

SymbolPtr TTY::getAlphaSym(const char& c) {
  const unsigned short cid = (unsigned short)c;
  if(cid >= sym.alphas.size()) return SymbolPtr(nullptr);
  else return sym.alphas[cid];
}
void TTY::addAlphaSym(const char& c) {
  const auto alAxisFunc = intrinBind(mkChanAlpha);
  const unsigned short cid = (unsigned short)c;
  if(cid >= sym.alphas.size()) {
    std::cerr << "tty alpha greater than sym.alpha size\n";
    return;
  }
  
  SymbolPtr alSym = this->requireSymbol(std::string(1, c), alAxisFunc);
  sym.alphas[cid] = alSym;
}

char TTY::symbolToChar(SymbolPtr ptr) {
    for(unsigned int i = 0; i < sym.alphas.size(); i++) {
        SymbolPtr csym = sym.alphas[i];
        if(csym == ptr) {
            return static_cast<char>(i);
        }
    }
    return char(0);
}
char TTY::nodeToChar(const SymbolNode* node) {
    pfloat score = 0.0; char ret = '?';
    for(auto it = node->cbegin(); it != node->cend(); ++it) {
        char res = this->symbolToChar(it->instance());
        if(res != 0 && it->probability() > score) {
            score = it->probability();
            ret = res;
        }
    }
    return ret;
}

_register(Intrinsic, TTY);

};