#ifndef TTY_H
#define TTY_H
#include "include.h"
#include "intrinsic/Intrinsic.h"

class SymbolNode;

namespace Intrin {

class TTY : public Intrinsic {
public:
    TTY();
    virtual ~TTY();
    
    int initialize() override;
    int terminate() override;
    
    void presentText(const std::string& file, float dT);
    
    SymbolPtr mkChanAlpha(const std::string& fullName, const std::string& n);
    char nodeToChar(const SymbolNode* node);
    
    static inline const std::string Name() {return "TTY";};
private:
    struct {
        std::vector<SymbolPtr> alphas;
    } sym;
    SymbolPtr getAlphaSym(const char& c);
    void addAlphaSym(const char& c);
    
public:
    inline SymbolPtr charToSymbol(const char& c) { return this->getAlphaSym(c); };
    char symbolToChar(SymbolPtr ptr);
};

};

#endif /* TTY_H */

