#ifndef INSTANCEVECTOR_H
#define INSTANCEVECTOR_H
#include "include.h"
#include <list>

#define INSTANCEVECTOR_TPL template<typename T, typename IND>

INSTANCEVECTOR_TPL
class InstanceVector {
public:
    InstanceVector();
    InstanceVector(const InstanceVector& orig);
    virtual ~InstanceVector();
    
    InstanceVector<T,IND> operator-() const;
    InstanceVector<T,IND> operator+(const InstanceVector<T,IND>& o) const;
    InstanceVector<T,IND> operator*(const InstanceVector<T,IND>& o) const;
    InstanceVector<T,IND> operator/(const InstanceVector<T,IND>& o) const;
    
    InstanceVector<T,IND>& operator+=(const InstanceVector<T,IND>& o);
    InstanceVector<T,IND>& operator-=(const InstanceVector<T,IND>& o);
    InstanceVector<T,IND>& operator*=(const InstanceVector<T,IND>& o);
    InstanceVector<T,IND>& operator/=(const InstanceVector<T,IND>& o);
    
    inline InstanceVector<T,IND> operator-(const InstanceVector<T,IND>& o) const {
        return (*this) + (-o);
    }
    
    typedef std::list<T> EntryList;
    EntryList entries;
    
    struct {
        bool isIdentity : 1;
        bool isSpecNegative : 1;
        int padd : 6;
    } flags;
    
    //On magnitude of individual components
    pfloat maxInstanceMagnitude(bool useAbs = false) const;
    pfloat minInstanceMagnitude(bool useAbs = false) const;
    inline pfloat maxInstanceMagnitudeAbs() const { return this->maxInstanceMagnitude(true); };
    inline pfloat minInstanceMagnitudeAbs() const { return this->minInstanceMagnitude(true); };
    
    pfloat magnitude() const;
    inline InstanceVector<T,IND> deltaTo(const InstanceVector<T,IND>& o) const {
        return o - (*this);
    }
    pfloat distanceTo(const InstanceVector<T,IND>& o) const {
        return this->deltaTo(o).magnitude();
    }
    //Score on the range 0 to 1, 1 being best with delta=0, and score->0 as delta->principle, pow curve
    pfloat scoreTo(const InstanceVector<T,IND>& o, const InstanceVector<T,IND>& principle, pfloat curve = 1.0) const;
    
    inline InstanceVector<T,IND> deltaFrom(const InstanceVector<T,IND>& o) const {
        return o.deltaTo(*this);
    }
    
    T* addInstance(const T& instance);
    T* getInstance(IND &ptr);
    const T* getInstance(const IND& ptr) const;
    void delInstance(const IND& ptr);
    
    inline bool hasInstance(const IND& ptr) const { return this->getInstance(ptr) != nullptr; };
    
    inline pfloat getValue(IND &ptr, pfloat defaultValue = pfloat(0.0)) const {
        const T *const ins = this->getInstance(ptr);
        if(ins == nullptr) return defaultValue;
        else return ins->val();
    }
    inline pfloat getValueAbs(IND &ptr, pfloat defaultValue = pfloat(0.0)) const { return fabs(this->getValue(ptr, defaultValue)); };
    
    inline typename EntryList::iterator begin() { return entries.begin(); };
    inline typename EntryList::iterator end() { return entries.end(); };
    inline typename EntryList::const_iterator cbegin() const { return entries.begin(); };
    inline typename EntryList::const_iterator cend() const { return entries.end(); };
    
    inline unsigned int size() const { return entries.size(); };
    
    void debugPrint(unsigned short lvl);
    std::string toString();
    
    inline static InstanceVector<T,IND> delta(const InstanceVector<T,IND>& a, const InstanceVector<T,IND>& b) {
        return a.deltaTo(b);
    }
    
    inline static InstanceVector<T,IND> Identity() {
        InstanceVector<T,IND> ret = InstanceVector<T,IND>();
        ret.flags.isIdentity = true;
        return ret;
    }
private:

};

#endif /* INSTANCEVECTOR_H */

