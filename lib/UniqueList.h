#ifndef UNIQUELIST_H
#define UNIQUELIST_H
#include <list>
#include <algorithm>
#include <functional>

#ifndef MERGE
#ifndef MERGE_
#define MERGE_(a,b)  a##b
#endif
#define MERGE(a,b) MERGE_(a,b)
#endif 

#define UniqueList_FNName(PT, FN, PFX) MERGE(PT,MERGE(PFX,FN))
#define UniqueList_AddFN(PT, FN) UniqueList_FNName(PT,FN,_ACS_)
#define UniqueList_DelFN(PT, FN) UniqueList_FNName(PT,FN,_RCS_)

#define UniqueList_Pre_Add(PARENT_T, T, FN) inline bool UniqueList_AddFN(PARENT_T,FN)(PARENT_T *, T);
#define UniqueList_Expose_Add(PARENT_T, T, FN) inline bool UniqueList_AddFN(PARENT_T,FN)(PARENT_T *parent, T v) {\
    return (parent->FN)(v);\
}
#define UniqueList_Pre_Del(PARENT_T, T, FN) inline void UniqueList_DelFN(PARENT_T,FN)(PARENT_T *, T);
#define UniqueList_Expose_Del(PARENT_T, T, FN) inline void UniqueList_DelFN(PARENT_T,FN)(PARENT_T *parent, T v) {\
    (parent->FN)(v);\
}

#define UniqueList_Pre(PARENT_T, T, AFN, RFN) UniqueList_Pre_Add(PARENT_T,T,AFN)\
UniqueList_Pre_Del(PARENT_T,T,RFN)

#define UniqueList_Expose(PARENT_T, T, AFN, RFN) UniqueList_Expose_Add(PARENT_T,T,AFN)\
UniqueList_Expose_Del(PARENT_T,T,RFN)

//template<typename T, bool (*ON_ADD)(T&) = UniqueList_NOP_ADD, void (*ON_DEL)(T&) = UniqueList_NOP_DEL, typename CONST_T = const T>
template<typename T, typename PARENT_T, bool (*ON_ADD)(PARENT_T*, T) = nullptr, void (*ON_REMOVE)(PARENT_T*, T) = nullptr, typename CONST_T = const T&>
class UniqueList {
public:
    typedef typename std::list<T>::iterator iterator;
    
    inline UniqueList() : entries(), parent(nullptr) {};
    inline UniqueList(PARENT_T* par) : entries(), parent(par) {};
    inline UniqueList(const UniqueList& orig) : entries(orig.entries), parent(nullptr) { };
    inline virtual ~UniqueList() {
        
    }
    
    std::list<T> entries;
    PARENT_T* parent;
    
    inline bool has(CONST_T& item) const {
        return this->find(item) != this->end();
    }
    inline bool add(T item) {
        auto it = this->find(item);
        if(it != this->end()) return true;
        
        bool res = (parent == nullptr || ON_ADD == nullptr) || (*ON_ADD)(parent, item);
        if(res) entries.push_back(item);
        return res;
    }
    inline bool remove(CONST_T item) {
        auto it = this->find(item);
        if(it == this->end()) return false;
        
        entries.erase(it);
        if(parent != nullptr && ON_REMOVE != nullptr) (*ON_REMOVE)(parent, item);
        return true;
    }
    
    inline iterator begin() { return entries.begin(); };
    inline iterator end() { return entries.end(); };
    inline iterator find(CONST_T item) {
        return std::find(this->begin(), this->end(), item);
    }
private:

};

#define UniqueListCB(T, PT, AFN, RFN)  UniqueList<T, PT, UniqueList_AddFN(PT,AFN), UniqueList_DelFN(PT, RFN)>

#endif /* UNIQUELIST_H */

