#ifndef REGISTRARNAMED_H
#define REGISTRARNAMED_H
#include "include.h"
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <functional>
#include <unordered_map>

template<typename CT>
class NamedRegistrar {
public:
    static std::vector<CT*> registry;
    typedef std::function<void(CT *const &)> RegistryAlteredCB;
    static std::vector<RegistryAlteredCB> registeredCB, unregisteredCB;
    static std::unordered_map<std::string, CT*> registryNameMap;
    
    inline NamedRegistrar() {
        
    }
    inline virtual ~NamedRegistrar() {
        for(auto it = registry.begin(); it != registry.end(); ++it) {
            delete (*it);
        }
        registry.clear();
    }
    
    inline virtual void registered() {
        
    }
    inline virtual void unregistered() {
        
    }
    
    inline static void Register(CT *const &instance) {
        registry.push_back(instance);
        instance->registered();
        registryNameMap[instance->getName()] = instance;
        for(const RegistryAlteredCB &callback : registeredCB) callback(instance);
    }
    inline static void Unregister(CT *const &instance) {
        auto it = std::find(registry.begin(), registry.end(), instance);
        if(it != registry.end()) {
            auto mapIt = registryNameMap.find(instance->getName());
            if(mapIt != registryNameMap.end()) registryNameMap.erase(mapIt);
            
            for(const RegistryAlteredCB &callback : unregisteredCB) callback(instance);
            
            (*it)->unregistered();
            registry.erase(it);
        }
        
    }
    
    inline static void AddRegisterCallback(const RegistryAlteredCB &func) {
        registeredCB.push_back(func);
    }
    inline static void AddUnregisterCallback(const RegistryAlteredCB &func) {
        unregisteredCB.push_back(func);
    }
    inline static bool HasRegisterCallbacks() {
        return registeredCB.size() > 0;
    }
    inline static bool HasUnregisterCallbacks() {
        return unregisteredCB.size() > 0;
    }
    
    inline static CT* ByName(const std::string& n) {
        auto it = registryNameMap.find(n);
        if(it == registryNameMap.end()) return nullptr;
        else return it->second;
    }
    template<typename T>
    static inline T* ByName(const std::string& n) {
        return (T*)ByName(n);
    }
    template<typename T>
    static inline T* ByType() {
        const std::string name = T::Name();
        return ByName<T>(name);
    }
    
    static inline const std::string Name() {
        return "UNDEFINED";
    }
    
protected:

private:
    
};

template<typename CT>  std::vector<CT*> _dalloc_pre() NamedRegistrar<CT>::registry;
template<typename CT>  std::unordered_map<std::string, CT*> _dalloc_pre() NamedRegistrar<CT>::registryNameMap;
template<typename CT>  std::vector<std::function<void(CT *const &)>> _dalloc_pre() NamedRegistrar<CT>::registeredCB;
template<typename CT>  std::vector<std::function<void(CT *const &)>> _dalloc_pre() NamedRegistrar<CT>::unregisteredCB;

#endif /* REGISTRARNAMED_H */