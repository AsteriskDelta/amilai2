#ifndef SOLIST_H
#define SOLIST_H
#include <list>

template <typename T, typename S>
struct SOListData {
    S score;
    T value;
    inline SOListData(const S& s, const T& v) : score(s), value(v) {};
    inline operator T&() { return value; };
    inline bool operator<(const SOListData<T,S>& o) {
        return score < o.score;
    }
};

template<typename S, typename T>
class SOList : public std::list<SOListData<T,S>> {
public:
    inline SOList(unsigned int len) : std::list<SOListData<T,S>>(), maxLength(len) {};
    inline SOList(const SOList<T,S>& orig) : std::list<SOListData<T,S>>(orig), maxLength(orig.maxLength) {};
    inline virtual ~SOList() {};
    
    inline S min() const {
        if(this->size() == 0) return S(0);
        else return this->back().score;
    }
    inline S max() const {
        if(this->size() == 0) return S(0);
        else return this->front().score;
    }
    
    inline T& push(S score, T& value) {
        if(this->size() >= maxLength) {
            if(score <= this->min()) return value;//Not good enough, skip
            else this->pop_back();
        }
        
        for(auto it = this->begin(); it != this->end(); it++) {
            if(score > it->score) return (this->insert(it, SOListData<T,S>(score, value)))->value;
            else if(score == it->score && value == it->value) return it->value;//Don't insert a duplicate
        }
        return (this->push_back(SOListData<T,S>(score, value)), this->back()).value;
    }
    inline void pop(T& value) {
        for(auto it = this->begin(); it != this->end(); it++) {
            if(value == it->value) {
                this->erase(it);
                return;
            }
        }
    }
    
    inline unsigned int size_max() const { return this->maxLength; };
    inline unsigned int max_size() const { return this->size_max(); };
private:
    unsigned int maxLength;
};

#endif /* SOLIST_H */

