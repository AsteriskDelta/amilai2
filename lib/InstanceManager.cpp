#include "InstanceManager.h"
#include "lib/Registrar.h"
#include <algorithm>
/*
static void AllocInstanceManager() {
    _globalSymbols = new InstanceManager(true);
    Console::Out("Global InstanceManager allocated at ", _globalSymbols);
}
_preregister(AllocInstanceManager);
*/
INSTANCEMANAGER_TPL
InstanceManager<IDT,T,LDT>::InstanceManager(bool isGlobal) : loadQueue(), loaded(), idAutoincrement(0), 
        isGlobalManager(isGlobal), parents(this), children(this) {
    if(isGlobal) this->useGlobalScheme();
    
}

INSTANCEMANAGER_TPL
InstanceManager<IDT,T,LDT>::~InstanceManager() {
}

INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::add(T sym) {
    if(this->get(sym.id())) return;//Already indexed
    
    sym->incIdx();//Increment index count
    loaded[sym.id()] = sym;
    if(sym->hasName()) namedRef[sym->getName()] = sym.id();
    
    for(InstanceManager* child : children) this->exportInstance(child, sym.id());
}

INSTANCEMANAGER_TPL
T InstanceManager<IDT,T,LDT>::get(IDT id) {
    auto it = loaded.find(id); T ret;
    if(it == loaded.end()) {
        ret = this->load(id);
    } else ret = it->second;
    
    return ret;
}

INSTANCEMANAGER_TPL
T InstanceManager<IDT,T,LDT>::get(const std::string& n) {
    auto it = namedRef.find(n);
    if(it == namedRef.end()) return T(nullptr);
    else return this->get(it->second);
}

INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::remove(IDT id) {
    auto it = loaded.find(id);
    if(it == loaded.end()) return;
    
    for(InstanceManager* child : children) this->unexportInstance(child, id);
    
    T sym = it->second;
    if(sym->hasName()) namedRef.erase(namedRef.find(sym->getName()));
    
    loaded.erase(it);
    sym->decIdx();//Lessen index count
}

INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::preload(IDT id) {
    LDT toAdd;
    toAdd.id = id;
    loadQueue.push(toAdd);
}

INSTANCEMANAGER_TPL
IDT InstanceManager<IDT,T,LDT>::getNewID() {
    if(isGlobalManager) return idAutoincrement--;
    else return idAutoincrement++;
}

INSTANCEMANAGER_TPL
T InstanceManager<IDT,T,LDT>::load(IDT id) {
    _unused(id);
    return T(nullptr);
}
INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::unload(IDT id) {
    _unused(id);
}

INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::exportAll(InstanceManager *other) {
    for(auto it = this->begin(); it != this->end(); ++it) {
        this->exportInstance(other, it->first);
    }
}
INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::unexportAll(InstanceManager *other) {
    for(auto it = this->begin(); it != this->end(); ++it) {
        this->unexportInstance(other, it->first);
    }
}
INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::exportInstance(InstanceManager *other, IDT symID) {
    other->add(this->get(symID));
}
INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::unexportInstance(InstanceManager *other, IDT symID) {
    other->remove(symID);
}

INSTANCEMANAGER_TPL
bool InstanceManager<IDT,T,LDT>::doAddParent(InstanceManager *parent) {
    parent->children.add(this);
    parent->exportAll(this);
    return true;
}
INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::doRemoveParent(InstanceManager *parent) {
    parent->children.remove(this);
    parent->unexportAll(this);
}

INSTANCEMANAGER_TPL
void InstanceManager<IDT,T,LDT>::useGlobalScheme() {
    isGlobalManager = true;
    idAutoincrement = (static_cast<IDT>(1) << (T::IDBitLength()-1) ) - 1;
}