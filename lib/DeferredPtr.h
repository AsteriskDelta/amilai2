#ifndef DEFERREDPTR_H
#define DEFERREDPTR_H
#include <stdint.h>
#include <iostream>

/* Header only with inline functions due to template evaluation error outside class body, g++ 6.2.1
 Cleanup pending on g++ 7

 WARNING: Assumes 2-byte alignment of CT
*/

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define DEFERREDPTR_RPTR_MASK  (~reinterpret_cast<uintptr_t>((uintptr_t)0x01))
#else//Little endian
#define DEFERREDPTR_RPTR_MASK ( ~(reinterpret_cast<uintptr_t>((uintptr_t)0x01) << (sizeof(uintptr_t)*8 - 1)) )
#endif

#ifdef DEFERREDPTR_DEBUG
#define DEFPTR_DBG_MSG(x) x
#else
#define DEFPTR_DBG_MSG(x)
#endif

//Works with optimization on x64 and ARM, may be broken on other architectures due to shared line caching- set to empty if needed, but for less performance
#define DEFERREDPTR_FAKE_CONST const
//Masked local value of IDT_NULL, which is (ie, IDT_NULL is) expected to be set as a sizeof(void*)*8 - 1 bit value by API user
#define DEFERREDPTR_LOCAL_IDT_NULL (IDT_NULL & DEFERREDPTR_RPTR_MASK)

template <typename CT, typename IDT, CT* (*FRES)(const IDT&), IDT IDT_NULL>
class DeferredPtr {
public:
    typedef DeferredPtr<CT,IDT,FRES,IDT_NULL> SELF;
    typedef unsigned long long PtrUInt;
    
    //Cannot use initializer lists on anon. union members, hence body initialization.
    inline DeferredPtr() {
        (*this) = IDT_NULL;
    }
    inline DeferredPtr(CT *const& npt) {
        (*this) = npt;
    }
    inline DeferredPtr(const IDT& nid) {
        (*this) = nid;
    }
    inline DeferredPtr(const DeferredPtr& orig) {
        raw = orig.raw;
    }
    inline virtual ~DeferredPtr() {
        
    }
protected:
    inline CT* getPtr(const bool& alreadyInvoked = false) const {
        if(!hasPtr()){
            if(alreadyInvoked) return nullptr;
            else return this->ptr();
        } else return reinterpret_cast<CT*>(reinterpret_cast<uintptr_t>(dPtr) & DEFERREDPTR_RPTR_MASK );
    }
    inline IDT getID() const {
        if(dID == DEFERREDPTR_LOCAL_IDT_NULL) return IDT_NULL;
        else return dID;
    }
public:
    inline IDT id() DEFERREDPTR_FAKE_CONST {
        if(this->hasPtr() && this->getPtr() == nullptr) return IDT_NULL;
        else if(this->hasPtr()) return ptr()->id();
        else return this->getID();
    }
    
    inline CT* ptr() DEFERREDPTR_FAKE_CONST {//Preserve (currently) unresolvable 
        SELF* ncThis = const_cast<SELF*>(this);
        if(ncThis->hasPtr()) return ncThis->getPtr();
        
        CT *const resolvedPtr = (*FRES)(ncThis->id());
        if(resolvedPtr != nullptr) (*ncThis) = resolvedPtr;
        return ncThis->getPtr(true);
    }
    
    inline bool resolve() {
        if(this->hasPtr()) return true;
        else return (this->ptr() != nullptr);
    }
    
    inline CT& operator*() DEFERREDPTR_FAKE_CONST {
        return *(this->ptr());
    }
    
    inline CT* operator->() DEFERREDPTR_FAKE_CONST {
        return (this->ptr());
    }
    
    inline SELF& operator=(const DeferredPtr& orig) {
        raw = orig.raw;
        return *this;
    }
    inline SELF& operator=(const IDT& nid) {
        ptrSec = false;//ptrUsed = false;
        dID = nid;
        return *this;
    }
    inline SELF& operator=(CT *const& pt) {
        DEFPTR_DBG_MSG(std::cout << "\tPTR ASSIGN " << pt << ", mask = " << (void*)DEFERREDPTR_RPTR_MASK << "\n");
        ptrUsed = true;
        rawPtr = reinterpret_cast<PtrUInt>(pt);
        return *this;
    }
    
    union {//Little-endian FTW
        union {
            CT *dPtr;
            struct {
                PtrUInt rawPtr : 63;
                bool ptrUsed : 1;
            };
        };
        struct {
            IDT dID : 63;
            bool ptrSec : 1;
        };
        unsigned long long raw;
    };
    
    inline bool hasPtr() const {
        return ptrUsed;
    }
    
    inline operator bool() DEFERREDPTR_FAKE_CONST {
        DEFPTR_DBG_MSG(std::cout << "\top bool(), hasPtr() = " << hasPtr() << ":\n\t\tgetPtr() = " << getPtr() << "\n\t\tgetID() = " << this->getID() << " (NULL="<< IDT_NULL <<")\n"); 
        //Although the ptr *could* be valid if this condition is true, the bool operator is made to guard around derefs, hence this behavior. See possible()
        //return (hasPtr() && getPtr() != nullptr) || (!hasPtr() && this->getID() != IDT_NULL);
        return this->ptr() != nullptr;
    }
    inline bool possible() const {
        return (hasPtr() && getPtr() != nullptr) || (!hasPtr() && this->getID() != IDT_NULL);
    }
    
    inline bool operator==(CT *const& o) DEFERREDPTR_FAKE_CONST {
        DEFPTR_DBG_MSG(std::cout << "\top CT* ==\t " << this->ptr() << " == " << o << "\n");
        return this->ptr() == o;
    }
    inline bool operator==(const IDT& o) const {
        DEFPTR_DBG_MSG(std::cout << "\top IDT ==\t " << this->id() << " == " << o << "\n");
        return this->id() == o;
    }
    inline bool operator==(const DeferredPtr& o) DEFERREDPTR_FAKE_CONST {
        DEFPTR_DBG_MSG(std::cout << "\top DEFPTR ==\t " << this->id() << " == " << o->id() << "\n");
        return this->id() == o->id();
    }
    
    static inline unsigned char IDBitLength() { return 63; };//How many bits of uint we get
    static inline unsigned char PtrAddressMultiple() { return 2; };//Must be a multiple of this to be adressable
    
    //SFINAE due to FAKE_CONST vs true const possible attribs, I know it's ugly ;-;
    template<typename T>
    inline typename std::enable_if<!std::is_same<T, CT*>::value, bool>::type operator!=(const T& o) const {
        return !(*this == o);
    }
    template<typename T>
    inline typename std::enable_if<std::is_same<T, CT*>::value, bool>::type operator!=(const T& o) DEFERREDPTR_FAKE_CONST {
        return !(*this == o);
    }
private:
    
};

#endif /* DEFERREDPTR_H */

