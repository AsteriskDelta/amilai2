#ifndef INSTANCEMANAGER_H
#define INSTANCEMANAGER_H
#include "include.h"
#include "lib/UniqueList.h"
#include <unordered_map>
#include <queue>
//UniqueList_Pre(InstanceManager, InstanceManager*, doAddParent, doRemoveParent);
#define InstanceManager_Pre(IDT,T,LDT) UniqueList_Pre(InstanceManager<IDT,T,LDT>, InstanceManager<IDT,T,LDT>*, doAddParent, doRemoveParent)
#define INSTANCEMANAGER_TPL template<typename IDT, typename T, typename LDT>

INSTANCEMANAGER_TPL
class InstanceManager;

INSTANCEMANAGER_TPL
inline bool InstanceManager_AddParent(InstanceManager<IDT,T,LDT> *ins, InstanceManager<IDT,T,LDT> *v) {
    return ins->doAddParent(v);
}
INSTANCEMANAGER_TPL
inline void InstanceManager_DelParent(InstanceManager<IDT,T,LDT> *ins, InstanceManager<IDT,T,LDT> *v) {
   ins->doRemoveParent(v);
}

INSTANCEMANAGER_TPL
class InstanceManager {
public:
    typedef InstanceManager<IDT,T,LDT> SELF;
    
    InstanceManager(bool isGlobal = false);
    InstanceManager(const InstanceManager<IDT,T,LDT>& orig) = delete;
    virtual ~InstanceManager();
    
    void add(T sym);
    void remove(IDT id);
    
    T get(IDT id);//Always returns a resolved sym
    T get(const std::string& n); 
    
    void preload(IDT id);
    
    IDT getNewID();
    
    void exportAll(InstanceManager *other);
    void unexportAll(InstanceManager *other);
    void exportInstance(InstanceManager *other, IDT symID);
    void unexportInstance(InstanceManager *other, IDT symID);
    
    bool doAddParent(InstanceManager *parent);
    void doRemoveParent(InstanceManager *parent);
    
    T load(IDT id);
    void unload(IDT id);
protected:
    void useGlobalScheme();//Call ONLY on global sym manager;
    
    std::queue<LDT> loadQueue;//Contains only resolved syms
    
    std::unordered_map<IDT, T> loaded;
    std::unordered_map<std::string, IDT> namedRef;
    
    IDT idAutoincrement;
    bool isGlobalManager;
public:
    //UniqueListCB(SELF*, SELF, doAddParent, doRemoveParent) parents;
    UniqueList<SELF*, SELF, InstanceManager_AddParent<IDT,T,LDT>, InstanceManager_DelParent<IDT,T,LDT>> parents;
    UniqueList<SELF*, SELF> children;
    
    inline typename std::unordered_map<IDT, T>::iterator begin() { return loaded.begin(); };
    inline typename std::unordered_map<IDT, T>::iterator end() { return loaded.end(); };
};

//UniqueList_Expose(InstanceManager, InstanceManager*, doAddParent, doRemoveParent);
#define InstanceManager_Post(IDT,T,LDT) UniqueList_Expose(InstanceManager<IDT,T,LDT>, InstanceManager<IDT,T,LDT>*, doAddParent, doRemoveParent)

#endif /* INSTANCEMANAGER_H */

