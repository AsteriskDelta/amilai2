#ifndef HASFUNCTION_H
#define HASFUNCTION_H

#define HAS_FUNCTION_PRE_NAME(NAME) _has_ ## NAME
#define HAS_FUNCTION_PRE(F) template <typename T>\
class HAS_FUNCTION_PRE_NAME(F) {\
    typedef char one;\
    typedef long two;\
    template <typename C> static one test( typeof(&C::F) ) ;\
    template <typename C> static two test(...);\
public:\
    enum { value = sizeof(test<T>(0)) == sizeof(char) };\
};

#define HAS_FUCNTION(T, F) (HAS_FUNCTION_PRE_NAME(F)<T>::value)

#endif /* HASFUNCTION_H */

