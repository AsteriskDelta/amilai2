#include "InstanceVector.h"
#include <limits>

INSTANCEVECTOR_TPL
InstanceVector<T,IND>::InstanceVector() : entries(), flags({false, false, 0}) {
}

INSTANCEVECTOR_TPL
InstanceVector<T,IND>::InstanceVector(const InstanceVector<T,IND>& orig) : entries(orig.entries), flags(orig.flags) {
    
}

INSTANCEVECTOR_TPL
InstanceVector<T,IND>::~InstanceVector() {
}

INSTANCEVECTOR_TPL
InstanceVector<T,IND> InstanceVector<T,IND>::operator-() const {
    InstanceVector<T,IND> ret;
    for(const T& sym: entries) {
        sym.instanceCopy(ret.addInstance(T(sym.instance(), sym.nval(), sym.probability()) ));
    }
    ret.flags.isSpecNegative = ~(this->flags.isSpecNegative);
    return ret;
}
INSTANCEVECTOR_TPL
InstanceVector<T,IND> InstanceVector<T,IND>::operator+(const InstanceVector<T,IND>& o) const {
    if(this->flags.isIdentity && !o.flags.isIdentity) return o;//Fix ordering for symbol filling (below)
    else if(o.flags.isIdentity) return *this;
    
    InstanceVector<T,IND> ret;
    for(const T& sym: entries) {
        const T *osym = o.getInstance(sym.instance());
        
        sym.instanceCopy(ret.addInstance(T(sym.instance(), sym.val() + (osym == nullptr? pfloat(0) : osym->val()),
                clamp((sym.probability()+(osym == nullptr? sym.probability() : osym->probability()) )/pfloat(2.0), pfloat(0.0), pfloat(1.0)) ) ));
    }
    
    for(const T& osym : o.entries) {
        if(this->getInstance(osym.instance()) != nullptr) continue;//Skip ones we already added above
         osym.instanceCopy(ret.addInstance(T(osym.instance(), osym.val(), osym.probability()) ));
    }
    return ret;
}
INSTANCEVECTOR_TPL
InstanceVector<T,IND> InstanceVector<T,IND>::operator*(const InstanceVector<T,IND>& o) const {
    if(this->flags.isIdentity && !o.flags.isIdentity) return o;//Fix ordering for symbol filling (below)
    else if(o.flags.isIdentity) return *this;
    
    InstanceVector<T,IND> ret;
    for(const T& sym: entries) {
        const T *osym = o.getInstance(sym.instance());
        if(osym == nullptr) continue;//zero times anything...
        
        sym.instanceCopy(ret.addInstance(T(sym.instance(), sym.val() * osym->val(), clamp(sym.probability()*osym->probability(), pfloat(0.0), pfloat(1.0)) ) ));
    }
    return ret;
}
INSTANCEVECTOR_TPL
InstanceVector<T,IND> InstanceVector<T,IND>::operator/(const InstanceVector<T,IND>& o) const {
    if(this->flags.isIdentity && !o.flags.isIdentity) return o;//Fix ordering for symbol filling (below)
    else if(o.flags.isIdentity) return *this;
    
    InstanceVector<T,IND> ret;
    for(const T& sym: entries) {
        const T *osym = o.getInstance(sym.instance());
        if(osym == nullptr) {//divide by zero? lolnope
            sym.instanceCopy(ret.addInstance(T(sym.instance(), std::numeric_limits<pfloat>::infinity(), clamp(sym.probability(), pfloat(0.0), pfloat(1.0)) ) ));
        } else {
            sym.instanceCopy(ret.addInstance(T(sym.instance(), sym.val() / osym->val(), clamp(sym.probability()/osym->probability(), pfloat(0.0), pfloat(1.0)) ) ));
        }
    }
    return ret;
}

INSTANCEVECTOR_TPL
pfloat InstanceVector<T,IND>::maxInstanceMagnitude(bool useAbs) const {
    if(flags.isIdentity) return pfloat(1.0);
    else if(entries.size() == 0) return pfloat(0.0);
    
    pfloat ret = std::numeric_limits<pfloat>::min();
    for(const T& sym : entries) {
        pfloat symMag = sym.magnitude();
        if(useAbs) symMag = fabs(symMag);
        ret = std::max(ret, symMag);
    }
    return ret;
}
INSTANCEVECTOR_TPL
pfloat InstanceVector<T,IND>::minInstanceMagnitude(bool useAbs) const {
    if(flags.isIdentity) return pfloat(1.0);
    else if(entries.size() == 0) return pfloat(0.0);
    
    pfloat ret = std::numeric_limits<pfloat>::max();
    for(const T& sym : entries) {
        pfloat symMag = sym.magnitude();
        if(useAbs) symMag = fabs(symMag);
        ret = std::min(ret, symMag);
    }
    return ret;
}

INSTANCEVECTOR_TPL
pfloat InstanceVector<T,IND>::magnitude() const {
    if(flags.isIdentity) return pfloat(1.0);
    
    pfloat ret = 0.0;
    for(const T& sym : entries) {
        const pfloat symMag = sym.magnitude();
        ret += symMag*symMag;
    }
    return sqrt(ret);
    
    //Honestly not sure what I was on to write that formula... but I want more ;)
    /*for(const T& sym : entries) {
        ret += sym.magnitude();
    }
    return pow(ret, pfloat(1.0)/pfloat(entries.size()));*/
}

INSTANCEVECTOR_TPL
pfloat InstanceVector<T,IND>::scoreTo(const InstanceVector<T,IND>& o, const InstanceVector<T,IND>& principle, pfloat curve) const {
    const InstanceVector<T,IND> delta = this->deltaTo(o);
    InstanceVector<T,IND> ratios = delta / principle;
    //Console::Out("\t\t\tratios=",ratios.toString(), " maxIMA=",ratios.maxInstanceMagnitudeAbs());
    //ratios approach "legs" of N-dimensional hypercube as delta->principle
    
    if(ratios.maxInstanceMagnitudeAbs() > pfloat(1.0)) return pfloat(0.0);//If we exceed any bounds, the test fails
    
    const pfloat maximumDistance = sqrt(pfloat(delta.size()));
    const pfloat actualDistance = ratios.magnitude();
    //Thus, actualDistance -> maximumDistance as delta->principle
    //Console::Out("\t\t\tactualDistance/maxDistance = ", actualDistance," / ", maximumDistance, " = ", actualDistance / maximumDistance);
    
    //And aD/mD -> 1 as delta->principle, and aD/mD -> 0 as delta->null, so take the reciprocal
    return pfloat(1.0) - pow(actualDistance / maximumDistance, curve);
}

INSTANCEVECTOR_TPL
T* InstanceVector<T,IND>::addInstance(const T& instance) {
  entries.push_back(instance);
  return &entries.back();
}
INSTANCEVECTOR_TPL
const T* InstanceVector<T,IND>::getInstance(const IND& ptr) const {
    for(auto it = entries.begin(); it != entries.end(); ++it) {
    if(it->instance() == ptr) {
      return &(*it);
    }
  }
  return nullptr;
}
INSTANCEVECTOR_TPL
T* InstanceVector<T,IND>::getInstance(IND &ptr) {
  for(auto it = entries.begin(); it != entries.end(); ++it) {
    if(it->instance() == ptr) {
      return &(*it);
    }
  }
  return nullptr;
}
INSTANCEVECTOR_TPL
void InstanceVector<T,IND>::delInstance(const IND &ptr) {
  for(auto it = entries.begin(); it != entries.end(); ++it) {
    if(it->instance() == ptr) {
        entries.erase(it);
        return;
    }
  }
  return;
}

INSTANCEVECTOR_TPL
void InstanceVector<T,IND>::debugPrint(unsigned short lvl) {
   const std::string prefix = std::string(lvl, std::string("\t")[0]);
   std::stringstream ss;
   for(T &ins : entries) ss <<((ins == *entries.begin())? "" : ", ") << ins.toString();
   
   if(flags.isIdentity) Console::Out(prefix, "InstanceVector<T,IND>(IDENTITY)");
   else Console::Out(prefix, "InstanceVector<T,IND>{", ss.str(), "}");
}
INSTANCEVECTOR_TPL
std::string InstanceVector<T,IND>::toString() {
   std::stringstream ss;
   for(T &ins : entries) ss <<((ins == *entries.begin())? "" : ", ") << ins.toString();
   
   if(flags.isIdentity) return "InstanceVector<T,IND>(IDENTITY)";
   else return "InstanceVector<T,IND>{"+ss.str()+"}";
}

INSTANCEVECTOR_TPL
InstanceVector<T,IND>& InstanceVector<T,IND>::operator+=(const InstanceVector<T,IND>& o) {
    *this = *this + o;
    return *this;
}
INSTANCEVECTOR_TPL
InstanceVector<T,IND>& InstanceVector<T,IND>::operator-=(const InstanceVector<T,IND>& o) {
    *this = *this - o;
    return *this;
}
INSTANCEVECTOR_TPL
InstanceVector<T,IND>& InstanceVector<T,IND>::operator*=(const InstanceVector<T,IND>& o) {
    *this = *this * o;
    return *this;
}
INSTANCEVECTOR_TPL
InstanceVector<T,IND>& InstanceVector<T,IND>::operator/=(const InstanceVector<T,IND>& o) {
    *this = *this / o;
    return *this;
}