#ifndef REGISTRAR_H
#define REGISTRAR_H
#include "include.h"
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <functional>

//Unnamed registrar
template<typename CT>
class Registrar {
public:
    static std::vector<CT*> registry;
    typedef std::function<void(CT *const &)> RegistryAlteredCB;
    static std::vector<RegistryAlteredCB> registeredCB, unregisteredCB;
    
    inline Registrar() {
        
    }
    inline virtual ~Registrar() {
        for(auto it = registry.begin(); it != registry.end(); ++it) {
            delete (*it);
        }
        registry.clear();
    }
    
    inline virtual void registered() {
        
    }
    inline virtual void unregistered() {
        
    }
    
    inline static void Register(CT *const &instance) {
        registry.push_back(instance);
        instance->registered();
        for(const RegistryAlteredCB &callback : registeredCB) callback(instance);
    }
    inline static void Unregister(CT *const &instance) {
        auto it = std::find(registry.begin(), registry.end(), instance);
        if(it != registry.end()) {
            for(const RegistryAlteredCB &callback : unregisteredCB) callback(instance);
            
            (*it)->unregistered();
            registry.erase(it);
        }
        
    }
    
    inline static void AddRegisterCallback(const RegistryAlteredCB &func) {
        registeredCB.push_back(func);
    }
    inline static void AddUnregisterCallback(const RegistryAlteredCB &func) {
        unregisteredCB.push_back(func);
    }
    inline static bool HasRegisterCallbacks() {
        return registeredCB.size() > 0;
    }
    inline static bool HasUnregisterCallbacks() {
        return unregisteredCB.size() > 0;
    }
    
    inline static void SetupIndicies() {
        
    }
    
protected:

private:
    
};

template<typename CT>  std::vector<CT*> _dalloc_pre() Registrar<CT>::registry;
template<typename CT>  std::vector<std::function<void(CT *const &)>> _dalloc_pre() Registrar<CT>::registeredCB;
template<typename CT>  std::vector<std::function<void(CT *const &)>> _dalloc_pre() Registrar<CT>::unregisteredCB;

//Named registrar
#include "RegistrarNamed.h"


template<typename CT>
class RegistrarProxy {
public:
    CT *instance;
    
    inline RegistrarProxy(CT *const newInstance) : instance(newInstance) {
        CT::Register(instance);
    };
    inline virtual ~RegistrarProxy(){
    };  
protected:
    
};

template<void F()>
class RegistrarFunctionProxy {
public:
    inline RegistrarFunctionProxy() {
        F();
    };
    inline ~RegistrarFunctionProxy(){
    };  
};

#define RGR_RNAME() EXT_REGISTRAR_ ## __LINE__
#define _register(T, I) static RegistrarProxy<T>  RGR_RNAME()  _dalloc(1000) (new I());
#define _preregister(F) static RegistrarFunctionProxy<F> RGR_RNAME()  _dalloc(800) ();
#define _register_da(T,I,L) static RegistrarProxy<T>  RGR_RNAME()  _dalloc(L) (new I());
#define _preregister_da(F,L) static RegistrarFunctionProxy<F> RGR_RNAME()  _dalloc(L) ();
#define _da_invoke(rl, F) static RegistrarFunctionProxy<F> RGR_RNAME()  _dalloc(rl) ();
#define _early_invoke(F) static RegistrarFunctionProxy<F> RGR_RNAME()  _early ();
#define REGISTRY_VARS(T) template class Registrar<T>
//template<> std::vector<T*> Registrar<T>::registry;

#endif /* REGISTRAR_H */

