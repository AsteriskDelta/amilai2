#include "HDIndex.h"
#include "include.h"

HDI_TPL
HDIndex HDI_SPC::HDIndex() : root(nullptr) {
    this->makeRoot();
}
HDI_TPL
HDIndex HDI_SPC::~HDIndex() {
    this->freeRoot();
}

HDI_TPL
void HDIndex HDI_SPC::add(NODE * const &node) {
    if (node == nullptr || root == nullptr) return;
    
    if(node->size() == 0) { DBG(Console::Warn("HDIndex::add rejected node with size=0, at ",node)); return; };
    if(node->isIndexed()) { DBG(Console::Warn("HDIndex::add already streamed node, at ",node)); return; };
    //rawNodes.emplace(node);
    root->emplace(node);
    node->setIndexed();
    processQueue.push(node);
}
HDI_TPL
void HDIndex HDI_SPC::remove(NODE * const &node) {
    if (node == nullptr || root == nullptr) return;
    if(node->size() == 0) { DBG(Console::Warn("HDIndex::remove rejected node with size=0, at ",node)); return; };
    if(!node->isIndexed()) { DBG(Console::Warn("HDIndex::remove not yet streamed node, at ",node)); return; };
    
    root->remove(node);
    node->clearIndexed();
    /*auto it = rawNodes.find(node);
  
    if(it != rawNodes.end()) {
      rawNodes.erase(node);
    }*/
}

HDI_TPL
NODE* HDIndex HDI_SPC::getUnprocessed() {
    if(processQueue.size() == 0) return nullptr;
    
    NODE *const ret = processQueue.front();
    processQueue.pop();
    return ret;
}

HDI_TPL
void HDIndex HDI_SPC::makeRoot() {
    root = new HDIPage HDI_SPC(nullptr, PTR(nullptr));
}
HDI_TPL
void HDIndex HDI_SPC::freeRoot() {
    if (root == nullptr) return;

    delete root;
}

HDI_TPL
void HDIndex HDI_SPC::debugPrint(unsigned short lvl) {
  if(root != nullptr) root->debugPrint(lvl);
}
/*HDI_TPL
NODE* HDIndex HDI_SPC::nearest(const VEC& vec, VAL min, VAL max) {
    const NearestList resList =  this->nearest(vec, VEC::Identity(), 1, min, max);
    if(resList.size() == 0) return nullptr;
    else return resList.front().value;
}*/
HDI_TPL
NODE* HDIndex HDI_SPC::nearest(const VEC& vec, const VEC& principle, typename HDIndex HDI_SPC::Options *const opts) {
    const NearestList resList =  this->nearestN(vec, principle, 1, opts);
    if(resList.size() == 0) return nullptr;
    else return resList.front().value;
}
/*HDI_TPL
typename HDIndex HDI_SPC::NearestList HDIndex HDI_SPC::nearest(const VEC& vec, unsigned int cnt, VAL min, VAL max) {
    return this->nearest(vec, VEC::Identity(), cnt, min, max);
}*/
HDI_TPL
typename HDIndex HDI_SPC::NearestList HDIndex HDI_SPC::nearestN(const VEC& vec, const VEC& principle, unsigned int cnt, typename HDIndex HDI_SPC::Options *const opts) {
    NearestList ret(cnt); 
    
    //Seperate codepaths for options allocation
    if(opts == nullptr) {
        Options optsInstance = Options();
        optsInstance.prepare();
        root->nearest(vec, principle, &ret, &optsInstance);
    } else {
        opts->prepare();
        root->nearest(vec, principle, &ret, opts);
    }
    
    return ret;
}