#include "HDIPage.h"
#include "HDIRange.h"
#include "hdi/HDIndex.h"
#include <limits>

HDI_TPL
HDIPage HDI_SPC::HDIPage(HDIPage HDI_SPC::Page *const par, PTR specializes) : parent(par), principles(), ranges(), nodeCount(0) {
    if(parent != nullptr) {
        unsigned short parentPrincipleCnt = parent->principles.size();
        principles.resize(parentPrincipleCnt+1);
        principles[0] = specializes;
        std::memcpy(&principles[1], &(parent->principles[0]), sizeof(PTR)*parentPrincipleCnt);
    }
}
HDI_TPL
HDIPage HDI_SPC::~HDIPage() {
    auto it = ranges.begin();
    while(it != ranges.end()) {
        auto next = std::next(it);
        this->deleteRange(it->first);
        it = next;
    }
}

HDI_TPL
bool HDIPage HDI_SPC::emplace(NODE *const & node) {
    if(node->isIndexed()) return false;
    bool ret = true;
    
    for(auto it = node->begin(); it != node->end(); it++) {
        auto sym = it->sym();//PTR (fakeconst or not)
        if(isPrinciple(sym)) continue;
        
        Range *range = this->getRange(sym, true);
        const bool empl = range->emplace(node);
        
        ret &= empl;
    }
    
    DBG(if(!ret) {
        Console::Warn("HDIPage HDI_SPC::emplace node at ",node," contains already indexed entries");
    });
    
    if(ret) nodeCount++;
    return ret;
}
HDI_TPL
bool HDIPage HDI_SPC::remove(NODE *const &node) {
    if(!node->isIndexed()) return false;
    bool ret = true;
    
    for(auto it = node->begin(); it != node->end(); it++) {
        auto sym = it->sym();//PTR (fakeconst or not)
        if(isPrinciple(sym)) continue;
        
        Range *range = this->getRange(sym, false);
        if(range != nullptr) ret &= range->remove(node);
        else {//Something went wrong (possible race condition, warn for development
            DBG(Console::Warn("\tHDIPage HDI_SPC::remove node at ",node," was not indexed (missing range) for ",sym.id()," at ",sym.ptr()));
            ret = false;
        }
    }
    
    DBG(if(!ret) {
        Console::Warn("HDIPage HDI_SPC::remove node at ",node," contains unindexed entries");
    });
    
    if(ret) nodeCount--;
    return ret;
}

HDI_TPL
bool HDIPage HDI_SPC::isPrinciple(PTR sym) {
    for(PTR& prinSym : principles) {
        if(prinSym == sym) return true;
    }
    return false;
}

HDI_TPL
typename HDIPage HDI_SPC::Range* HDIPage HDI_SPC::makeRange(PTR sym) {
    auto it = ranges.find(sym.id());
    if(it != ranges.end()) return it->second;
    
    return (ranges[sym.id()] = new Range(this, sym));
    
}
HDI_TPL
void HDIPage HDI_SPC::deleteRange(PTR sym) {
    auto it = ranges.find(sym.id());
    if(it == ranges.end()) return;
    
    delete it->second;
    ranges.erase(it);
}

HDI_TPL
void HDIPage HDI_SPC::prune() {
    
}
HDI_TPL
ID HDIPage HDI_SPC::size() const {
  return nodeCount;
}

HDI_TPL
void HDIPage HDI_SPC::debugPrint(unsigned short lvl) {
  const std::string prefix = std::string(lvl, std::string("\t")[0]);
  std::stringstream specList;
  for(PTR& ptr : principles) specList << ((ptr == principles[0])? "" : ", ") << ptr.id();
  
  Console::Out(prefix, "HDIPage[",nodeCount,"] from ", parent,", spec={",specList.str(),"}");
  for(auto it : ranges) {
    it.second->debugPrint(lvl+1);
  }
}

HDI_TPL
void HDIPage HDI_SPC::nearest(const VEC& vec, const VEC& principle, typename HDIndex HDI_SPC::NearestList *ll, typename HDIndex HDI_SPC::Options *const opts) {
    Range *bestRange = nullptr; ID bestSize = std::numeric_limits<idint>::max();
    for(auto it = vec.cbegin(); it != vec.cend(); ++it) {
        if(isPrinciple(it->instance())) continue;//We've already searched these implicitly
        
        //this->nearestOnRange(it->instance(), vec, principle, ll, opts);
        //if(opts->satisfied(ll)) return;
        Range *const range = this->getRange(it->instance());
        if(range == nullptr || range->size() > bestSize) continue;
        
        bestSize = range->size();
        bestRange = range;
    }
    
    //Only search the smallest range, since it still has all relevant points indexed
    if(bestRange == nullptr) return;
    bestRange->nearest(vec, principle, ll, opts);
    
    //We don't actually need to check these, because no projection of points without vec's components will be reachable
    /*for(auto it = principle.cbegin(); it != principle.cend(); ++it) {
        if(vec.hasInstance(it->instance())) continue;//Skip what we already checked
        this->nearestOnRange(it->instance(), vec, principle, ll, min, max);
    }*/
}

HDI_TPL
void HDIPage HDI_SPC::nearestOnRange(PTR rangeSym, const VEC& vec, const VEC& principle,  typename HDIndex HDI_SPC::NearestList *ll, typename HDIndex HDI_SPC::Options *const opts) {
    Range *const range = this->getRange(rangeSym);
    if(range == nullptr) return;
    
    //Console::Out("\tChecking range for ",rangeSym->idText());
    range->nearest(vec, principle, ll, opts);
}