#ifndef HDISUBSET_H
#define HDISUBSET_H
#include "include.h"
#include "HDIndex.h"
#include <set>

HDI_TPL
class HDISubset {
public:
    typedef HDIndex HDI_SPC Index;
    typedef HDIPage HDI_SPC Page;
    typedef HDIRange HDI_SPC _Range;
    typedef HDISubset HDI_SPC Subset;
    typedef typename std::set<Subset*>::iterator SubsetIter;
    
    struct Range {
        SubsetIter min, max;
        inline Range() : min(), max() {};
        inline Range(const SubsetIter& a, const SubsetIter&b) : min(a), max(b) {};
        inline SubsetIter begin() const { return min; };
        inline SubsetIter end() const { return max; };
        
        inline Subset* resolve(const SubsetIter& it) const { return (*it); };
    };
    
    HDISubset(VAL nMin, VAL nMax, Page *parent, PTR spec);
    virtual ~HDISubset();

    VAL min, max;
    mutable Page *page;
    mutable unsigned int nodeCount;

    inline operator VAL() const {
        return min;
    };

    inline bool operator<(const Subset& o) const {
        return VAL(*this) < VAL(o);
    }

    inline unsigned int size() const {
        return nodeCount;
    };

    bool emplace(NODE * const & node) const;
    bool remove(NODE * const &node) const;
    
    void nearest(const VEC& vec, const VEC& principleVector, typename Index::NearestList *ll, typename HDIndex HDI_SPC::Options *const opts);
private:

};

#endif /* HDISUBSET_H */

