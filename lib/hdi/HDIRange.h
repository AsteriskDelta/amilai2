#ifndef HDIRANGE_H
#define HDIRANGE_H
#include "include.h"
#include "HDIndex.h"
#include <set>
#include <list>
#include "HDISubset.h"

HDI_TPL
class HDIRange {
public:
    typedef HDIndex HDI_SPC Index;
    typedef HDIPage HDI_SPC Page;
    typedef HDIRange HDI_SPC Range;
    typedef HDISubset HDI_SPC Subset;
    
    typedef std::set<Subset*> Subsets;
    typedef typename Subsets::iterator SubsetIter;
    
    HDIRange(Page *par, PTR prin);
    virtual ~HDIRange();
    
    Page *parent;
    PTR principle;
    
    Subsets subsets;
    std::list<NODE*> freeNodes;
    ID nodeCount;
    
    bool emplace(NODE *const &node);
    bool remove(NODE *const &node);
    
    ID size() const;
    
    typename Subset::Range getRangeIterators(VAL vMin, VAL vMax);
    SubsetIter getSubset(VAL v, bool createIfNeeded = false);
    
    void nearest(const VEC& vec, const VEC& principleVector, typename Index::NearestList *ll, typename HDIndex HDI_SPC::Options *const opts);
    
    inline ID id() const {
        return principle.id();
    }
    
    void debugPrint(unsigned short lvl);
    
    inline bool hasFreeNodes() { return freeNodes.size() > 0; };
protected:
    Subset* takeSubset(const SubsetIter& it);
    SubsetIter returnSubset(Subset *const sub);
    
    SubsetIter makeSubset(VAL vMin, VAL vMax);
    void deleteSubset(SubsetIter it);
    void mergeSubset(SubsetIter a, SubsetIter b);
    void splitSubset(SubsetIter it);
    void rebalance(SubsetIter center);
    
private:

};

#endif /* HDIRANGE_H */

