#include "HDIRange.h"

//Min must be less than max/2 (for splitting)
#define SR_L3_MAX (2048)
#define SR_L3_MIN (SR_L3_MAX/2 - 8)

HDI_TPL
HDIRange HDI_SPC::HDIRange(HDIRange HDI_SPC::Page *par, PTR prin) : parent(par), principle(prin), subsets(), freeNodes(), nodeCount(0) {
  
}
HDI_TPL
HDIRange HDI_SPC::~HDIRange() {
  for(SubsetIter it = subsets.begin(); it != subsets.end(); ++it) {
    deleteSubset(it);
  }
  subsets.clear();
}

HDI_TPL
bool HDIRange HDI_SPC::emplace(NODE *const &node) {
  const VAL value = node->getInstance(principle)->value;
  
  if(this->size() < SR_L3_MAX) {//Simple free list
    freeNodes.push_back(node);
  } else {
    //SubsetIter it = this->getSubset(value, true);
    Console::Err("Streamrange unimplemented emplace case");
  }
  
  nodeCount++;
  return true;
}
HDI_TPL
bool HDIRange HDI_SPC::remove(NODE *const &node) {
  if(freeNodes.size() > 0) {
    for(auto it = freeNodes.begin(); it != freeNodes.end(); ++it) {
      if(node == *it) {
        freeNodes.erase(it);
        nodeCount--;
        return true;
      }
    }
  }
  
  const VAL value = node->getInstance(principle)->value;
  SubsetIter it = this->getSubset(value, false);
  if(it == subsets.end()) return false;
  
  const Subset* sub = *it;
  
  bool ret = sub->remove(node);
  if(ret) nodeCount--;
  return ret;
}

HDI_TPL
ID HDIRange HDI_SPC::size() const {
  return nodeCount;
}

HDI_TPL
typename HDISubset HDI_SPC::Range HDIRange HDI_SPC::getRangeIterators(VAL vMin, VAL vMax) {
    typename Subset::Range ret;
    ret.min = subsets.begin();
    ret.max = subsets.end();
    return ret;
}

HDI_TPL
void HDIRange HDI_SPC::nearest(const VEC& vec, const VEC& principleVector, typename HDIndex HDI_SPC::NearestList *ll, typename HDIndex HDI_SPC::Options *const opts) {
    if(this->hasFreeNodes()) {//We must evaluate all free nodes
        //Console::Out("\t\tRange ",principle->idText()," has ",freeNodes.size()," free nodes, checking them...");
        for(NODE* node : freeNodes) {
            const pfloat scoreCurve = pfloat(1.0);
            pfloat nodeScore = vec.scoreTo((VEC)*node, principleVector, scoreCurve);
            //Console::Out("\t\t\tNode got score of ",nodeScore, " must be ",min," <= x <= ",max);
            if(nodeScore == VAL(0.0) || nodeScore < opts->min || nodeScore > opts->max) continue;
            
            //Console::Out("\t\tMeets specs!!! ",nodeScore);
            ll->push(nodeScore, node);
            if(opts->satisfied(ll)) return;
        }
    }
    
    //And then ranges that could possibly contain our value
    const VAL vMin = vec.getValue(principle) - principleVector.getValueAbs(principle),
              vMax = vec.getValue(principle) + principleVector.getValueAbs(principle);
    typename Subset::Range searchRange = this->getRangeIterators(vMin, vMax);
    
    for(auto it = searchRange.begin(); it != searchRange.end(); ++it) {
        Subset* subset = searchRange.resolve(it);
        
        subset->nearest(vec, principleVector, ll, opts);
        if(opts->satisfied(ll)) return;
    }
}

HDI_TPL
typename HDIRange HDI_SPC::SubsetIter HDIRange HDI_SPC::getSubset(VAL v, bool createIfNeeded) {
  _unused(createIfNeeded);
  
  return subsets.begin();
}

HDI_TPL
typename HDIRange HDI_SPC::SubsetIter HDIRange HDI_SPC::makeSubset(VAL vMin, VAL vMax) {
  
}
HDI_TPL
void HDIRange HDI_SPC::deleteSubset(SubsetIter it) {
  
}
HDI_TPL
void HDIRange HDI_SPC::mergeSubset(SubsetIter a, SubsetIter b) {
  
}
HDI_TPL
void HDIRange HDI_SPC::splitSubset(SubsetIter it) {
  
}
HDI_TPL
void HDIRange HDI_SPC::rebalance(SubsetIter center) {
  
}

HDI_TPL
HDISubset HDI_SPC* HDIRange HDI_SPC::takeSubset(const SubsetIter& it) {
  return const_cast<Subset*>(*it);
}
HDI_TPL
typename HDIRange HDI_SPC::SubsetIter HDIRange HDI_SPC::returnSubset(Subset *const sub) {
  auto it = subsets.find(sub);
  subsets.erase(it);
  subsets.emplace(sub);
  return subsets.find(sub);
}

HDI_TPL
void HDIRange HDI_SPC::debugPrint(unsigned short lvl) {
   const std::string prefix = std::string(lvl, std::string("\t")[0]);
   Console::Out(prefix,"HDIRange(",principle->idText(),")[",this->size(),"]: ");
   if(freeNodes.size() > 0) {
     Console::Out(prefix,"  freeNodes:");
     for(NODE* nd : freeNodes) {
       nd->debugPrint(lvl+1);
     }
   }
}