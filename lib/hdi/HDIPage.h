#ifndef HDIPAGE_H
#define HDIPAGE_H
#include "HDIndex.h"
#include "include.h"
#include <unordered_map>
#include <vector>

HDI_TPL
class HDIPage {
public:
    typedef HDIndex HDI_SPC Index;
    typedef HDIPage HDI_SPC Page;
    typedef HDIRange HDI_SPC Range;
    typedef HDISubset HDI_SPC Subset;
    
    HDIPage(Page *const par, PTR specializes);
    virtual ~HDIPage();
    
    Page *parent;
    std::vector<PTR> principles;//With 0 being this page's principle
    
    //Maps symbol instance's symbol IDs to indexed ranges
    std::unordered_map<ID, Range*> ranges;
    
    ID nodeCount;
    
    bool emplace(NODE *const & node);//"Always" works, but was it already there? (false if so, or true if it's new)
    bool remove(NODE *const &node);//Return: did we actually remove something?
    
    void nearest(const VEC& vec, const VEC& principle, typename Index::NearestList *ll, typename HDIndex HDI_SPC::Options *const opts);
    
    ID size() const;
    
    void debugPrint(unsigned short lvl);
protected:
    Range* makeRange(PTR sym);
    void deleteRange(PTR sym);
    
    bool isPrinciple(PTR sym);
    
    void nearestOnRange(PTR rangeSym, const VEC& vec, const VEC& principle, typename Index::NearestList *ll, typename HDIndex HDI_SPC::Options *const opts);
    
    void prune();
    
    inline Range* getRange(PTR sym, bool create = false) {
        auto it = ranges.find(sym.id());
        if(it == ranges.end()) {
            if(create) return this->makeRange(sym.id());
            else return nullptr;
        } else return it->second;
    }
    
};

#endif /* HDIPAGE_H */

