#ifndef HDINDEX_H
#define HDINDEX_H

#include "lib/SOList.h"
#include "lib/MTQueue.h"

//Index type, ptr type, node type, and value type
#define HDI_TPL template<typename ID, typename PTR, typename NODE, typename VAL, typename VEC>
#define HDI_SPC <ID,PTR,NODE,VAL, VEC>
HDI_TPL class HDIPage;
HDI_TPL class HDIRange;
HDI_TPL class HDISubset;

HDI_TPL 
class HDIndex {
public:
    typedef HDIndex HDI_SPC Index; typedef Index Self;
    typedef HDIPage HDI_SPC Page;
    typedef HDIRange HDI_SPC Range;
    typedef HDISubset HDI_SPC Subset;
    
    typedef SOList<VAL, NODE*> NearestList;
    
    struct Options {
        VAL min, max;
        VAL satisfict;
        
        inline Options() : min(-1.0), max(-1.0), satisfict(VAL(-1.0)) {};
        inline void prepare() {
            if(min == VAL(-1.0)) min = VAL(0.0);
            if(max == VAL(-1.0)) max = VAL(1.0);
            if(satisfict == VAL(-1.0)) satisfict = VAL(1.0);
        }
        inline bool satisfied(NearestList *const ll) const {
            return ll->size() == ll->size_max() && (satisfict != VAL(-1.0) && ll->min() >= satisfict);
        }
    };
    
    HDIndex();
    virtual ~HDIndex();
    
    void add(NODE *const &node);
    void remove(NODE *const &node);
    
    NODE* getUnprocessed();
    
    //NODE* nearest(const VEC& vec, VAL min = VAL(-1.0), VAL max = VAL(-1.0));
    NODE* nearest(const VEC& vec, const VEC& principle, Options *const opts = nullptr);
    //NearestList nearest(const VEC& vec, unsigned int cnt, VAL min = VAL(-1.0), VAL max = VAL(-1.0));
    NearestList nearestN(const VEC& vec, const VEC& principle, unsigned int cnt, Options *const opts = nullptr);
    
    Page* root;
    
    void debugPrint(unsigned short lvl = 0);
protected:
    void makeRoot();
    void freeRoot();
    
    MTQueue<NODE*> processQueue;
private:

};

#endif /* HDINDEX_H */

