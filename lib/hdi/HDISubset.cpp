#include "HDISubset.h"
#include "HDIRange.h"

HDI_TPL
HDISubset HDI_SPC::HDISubset(VAL nMin, VAL nMax, Page *parent, PTR spec) : min(nMin), max(nMax), nodeCount(0) {
  page = new Page(parent, spec);
}
HDI_TPL
HDISubset HDI_SPC::~HDISubset() {
  if(page != nullptr) delete page;
}

HDI_TPL
bool HDISubset HDI_SPC::emplace(NODE *const & node) const {
  return page != nullptr && ++nodeCount && page->emplace(node);
};
HDI_TPL
bool HDISubset HDI_SPC::remove(NODE *const &node) const {
  return page != nullptr  && nodeCount-- && page->remove(node);
};

HDI_TPL
void HDISubset HDI_SPC::nearest(const VEC& vec, const VEC& principleVector, typename HDIndex HDI_SPC::NearestList *ll, typename HDIndex HDI_SPC::Options *const opts) {
    if(page == nullptr) return;
    
    page->nearest(vec, principleVector, ll, opts);
}