#ifndef AIINSTANCE_H
#define AIINSTANCE_H
#include "include.h"
#include "lib/RegistrarNamed.h"

class SymbolManager;
class RelationManager;
class Stream;

class AIInstance : public NamedRegistrar<AIInstance>{
public:
    AIInstance(const std::string& nName);
    virtual ~AIInstance();
    
    std::string name;
    
    struct {
        SymbolManager *symbols;
        RelationManager *relations;
        Stream *stream;
    } ai;
    struct {
        bool isInitialized : 1;
        int padd : 7;
    } flags;
    
    void initialize();
    void terminate();
    
    //Task switching on multiple AIs per thread
    void threadBegin();
    void threadEnd();
    
    bool isActive() const;
    
    inline const std::string& getName() const {
        return this->name;
    }
private:
    static unsigned int aiInstanceCnt;
};

#endif /* AIINSTANCE_H */

