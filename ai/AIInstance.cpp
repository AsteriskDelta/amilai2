#include "AIInstance.h"

#include "symbol/SymbolExt.h"
#include "relation/RelationExt.h"
#include "stream/StreamExt.h"

__thread AIInstance* _activeAIInstance = nullptr;
template class NamedRegistrar<AIInstance>;

AIInstance::AIInstance(const std::string& nName) : name(nName) {
}

AIInstance::~AIInstance() {
    if(flags.isInitialized) this->terminate();
}

void AIInstance::initialize() {
    ai.symbols = new SymbolManager();
    ai.symbols->parents.add(_globalSymbols);
    
    ai.relations = new RelationManager();
    ai.relations->parents.add(_globalRelations);
    
    ai.stream = new Stream();
    
    flags.isInitialized = true;
}
void AIInstance::terminate() {
    flags.isInitialized = false;
    delete ai.stream;
    delete ai.relations;
    delete ai.symbols;
}

void AIInstance::threadBegin() {
    _activeAIInstance = this;
    _symbols = ai.symbols;
    _relations = ai.relations;
    _stream = ai.stream;
}
void AIInstance::threadEnd() {
    _stream = nullptr;
    _relations = nullptr;
    _symbols = nullptr;
    _activeAIInstance = nullptr;
}

bool AIInstance::isActive() const {
    return _activeAIInstance == this;
}