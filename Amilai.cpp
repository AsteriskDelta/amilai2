#include "include.h"
#include "Amilai.h"
//#include "lib/Application.h"
#include "intrinsic/Intrinsics.h"
#include "testcase/TestCases.h"

#include "bootstrap/main.cpp"
#include "relation/RelationExt.h"

int proxy_main(int argc, char** argv) {
    _unused(argc); _unused(argv);
    
    AIInstance *aiInstance = new AIInstance("main"),
            *aiDual = new AIInstance("dual");
    aiInstance->initialize();
    aiDual->initialize();
    aiInstance->threadBegin();

    //auto *vis = Intrinsic::ByType<Intrin::Vision>();
    //auto *ocr = TestCase::ByType<AITest::BasicOCR>();
    auto *tty = Intrinsic::ByType<Intrin::TTY>();
    auto *time = Intrinsic::ByType<Intrin::Time>();

    tty->presentText("testcase/lit/excerpt.txt", 0.1f);
    //_stream->debugPrint();
    
    SymbolVector samplePos, sampleRange, ePos, eRange, timePos, allRange, tKeep;
    samplePos.addInstance(SymbolInstance(tty->charToSymbol('h'), 0.0));
    ePos.addInstance(SymbolInstance(tty->charToSymbol('e'), 0.0));
    ePos.addInstance(SymbolInstance(time->timeSym(), 0.1));
    timePos.addInstance(SymbolInstance(time->timeSym(), 0.0));
    
    
    tKeep.addInstance(SymbolInstance(time->timeSym(), pfloat(1.0)));
    
    sampleRange.addInstance(SymbolInstance(tty->charToSymbol('h'), 0.1));
    sampleRange.addInstance(SymbolInstance(time->timeSym(), std::numeric_limits<pfloat>::infinity()));
    
    eRange.addInstance(SymbolInstance(tty->charToSymbol('e'), 0.1));
    eRange.addInstance(SymbolInstance(time->timeSym(), 0.05));
    
    allRange.addInstance(SymbolInstance(time->timeSym(), std::numeric_limits<pfloat>::infinity()));
    
    Relation rel = Relation();
    rel.llOrder = 1;
    rel.nodes.resize(2);
    Relation::NodeData* relND;
    relND = &(rel.nodes[0]); relND->offset = samplePos; relND->eigen = sampleRange;
    relND = &(rel.nodes[1]); 
        relND->offset = ePos; relND->eigen = eRange;
        relND->order.resize(1);
        relND->order[0].mult = tKeep;
    
    /*Console::Out("Evaluating proto relation..."); SymbolNode* node;
    while((node = _stream->getUnprocessed()) != nullptr) {
        std::vector<SymbolNode*> nodesList = {node};
        pfloat relScore = rel.evaluate(nodesList);
        //std::cout << "rs " << relScore << "\n";
        if(relScore > 0.1) {
            Console::Out("\tstreamnode got ",relScore, " at ", node->toString());
        }
    }*/
    
    Stream::NearestList resultList = _stream->nearestN(samplePos, sampleRange, 200);
    
    Console::Out("");
    Console::Out("Results of query [",resultList.size(),"/",resultList.max_size(),"]:");
    for(auto it = resultList.begin(); it != resultList.end(); ++it) {
        Console::Out("\t",it->score*100.0,"%\t\t", it->value->toString());
        std::vector<SymbolNode*> nodesList = {it->value};
        RelationInstance relEval = rel.evaluate(nodesList);
        //std::cout << "rs " << relScore << "\n";
        if(relEval.sig() > 0.1) {
            Console::Out("\tstreamnode got \"",tty->nodeToChar(nodesList[0]),tty->nodeToChar(nodesList[1]),
                    "\": ",relEval.sig(), " inst= ", relEval.toString());
        }
    }
    Console::Out("");
    //Console::Out("BasicOCR: ", ocr->execute());

    //vis->presentImage("testcase/ocr/cases/a0.png");
    aiInstance->threadEnd();

    /*
    Console::Out("\nSwitching to dual instance...\n");

    aiDual->threadBegin();
    tty->presentText("testcase/lit/excerpt2.txt", 0.1f);
    _stream->debugPrint();
    aiDual->threadEnd();
*/
    Console::Out("Cleaning up to exit...");
    delete aiInstance;
    delete aiDual;
    
    return 0;
}