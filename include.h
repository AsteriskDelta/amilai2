#ifndef AMILAI_INCLUDE_H
#define AMILAI_INCLUDE_H

#include <ARKE/Shared.h>

#include <fstream>
#include <iostream>
#include <cstring>
#include <string>
#include <limits>
#include <algorithm>
#include <stdio.h>
#include <ctype.h>
#include <cstdlib>

#ifndef _allocl
#define _allocl(x) __attribute__ ((init_priority (x)))
#endif

#include <sstream>
#include <utility>

typedef float pfloat;
typedef unsigned long long idint;

#define IDINT_NONE ((unsigned long long)0xFFFFFFFFFFFFFFFF)

class Stream;
extern thread_local Stream *_stream;

class SymbolManager; class RelationManager;
extern SymbolManager *_globalSymbols;
extern RelationManager *_globalRelations;

extern __thread SymbolManager *_symbols;
extern __thread RelationManager *_relations;

#include <ARKE/Console.h>

#define DBG(x) x

#endif /* INCLUDE_H */

