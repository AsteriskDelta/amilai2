#include "SymbolResolve.h"
#include <ARKE/Console.h>
#include "SymbolManager.h"

Symbol* SymbolResolve(const idint& id) {
  //Console::Out("SymbolResolve: Asked to resolve ",id);
  return _symbols->get(id).ptr();
  //return nullptr;
}