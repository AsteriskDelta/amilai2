#ifndef SYMBOLPTR_H
#define SYMBOLPTR_H
#include "include.h"
#include "lib/DeferredPtr.h"
#include "SymbolResolve.h"
class Symbol;
//typedef Symbol* SymbolPtr;


typedef DeferredPtr<Symbol, idint, SymbolResolve, IDINT_NONE> SymbolPtr;
/*
class SymbolPtr {
public:
    SymbolPtr();
    SymbolPtr(const SymbolPtr& orig);
    virtual ~SymbolPtr();
private:

};*/

#endif /* SYMBOLPTR_H */

