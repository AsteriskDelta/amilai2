#include "SymbolInstance.h"

SymbolInstance::SymbolInstance() : symbol(nullptr), value(0.0), prob(0.0) {
    this->init();
}
SymbolInstance::SymbolInstance(SymbolPtr sym, pfloat v, pfloat prb) : symbol(sym), value(v), prob(prb) {
    this->init();
}
SymbolInstance::SymbolInstance(const SymbolInstance& orig) : symbol(orig.symbol), value(orig.value), prob(orig.prob) {
    this->init();
}

SymbolInstance::~SymbolInstance() {
    this->symbolUnset();
}

void SymbolInstance::init() {
    this->symbolSet();
}

void SymbolInstance::symbolSet() {
    if(symbol == nullptr) return;
    
    symbol->incRef();
}

void SymbolInstance::symbolUnset() {
    if(symbol == nullptr) return;
    symbol->decRef();
    symbol = nullptr;
}

SymbolInstance& SymbolInstance::operator=(const SymbolInstance& o) {
  this->symbolUnset();
  symbol = o.symbol;
  value = o.value;
  prob = o.prob;
  this->symbolSet();
  return *this;
}