#ifndef SYMBOLPROTOTYPE_H
#define SYMBOLPROTOTYPE_H
#include "include.h"
#include "Symbol.h"
#include "SymbolPtr.h"
#include "relation/RelationPtr.h"
#include <list>

class SymbolNode;

class SymbolPrototype {
public:
    struct Evaluation {
        struct Node {
            RelationPtr relation;
            SymbolNode *node;
            pfloat contribution;
        };
        std::list<Node> nodes;
        
        pfloat prob;
    };
    
    SymbolPrototype();
    virtual ~SymbolPrototype();
    
    std::list<RelationPtr> relations;
    
    pfloat evaluate(std::list<SymbolNode*> hints, Evaluation *&output);
private:

};

#endif /* SYMBOLPROTOTYPE_H */

