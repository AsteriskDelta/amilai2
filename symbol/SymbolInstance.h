#ifndef SYMBOLINSTANCE_H
#define SYMBOLINSTANCE_H
#include "include.h"
#include "Symbol.h"
#include "SymbolPtr.h"
#include <sstream>

class SymbolInstance {
public:
    SymbolInstance();
    SymbolInstance(SymbolPtr sym, pfloat val = 0.0, pfloat prb = 1.0);
    SymbolInstance(const SymbolInstance& o);
    virtual ~SymbolInstance();
    
    SymbolInstance& operator=(const SymbolInstance& o);
    
    SymbolPtr symbol;
    pfloat value;
    pfloat prob;
    
    inline SymbolPtr sym() const { return symbol; };
    inline SymbolPtr instance() const { return symbol; };
    
    inline pfloat val() const { return value; };
    inline pfloat nval() const { return -value; };
    inline pfloat probability() const { return prob; };
    
    inline bool operator==(const SymbolInstance& o) const {
        return symbol == o.symbol && value == o.value && prob == o.prob;
    }
    inline bool operator !=(const SymbolInstance& o) const { return !(*this == o); };
    
    inline pfloat magnitude() const {
        return value * prob;
    }
    
    inline std::string toString() const {
        std::stringstream ss;
        
        if(value != 0.0 || prob != 1.0) ss << "SymI(" << symbol->idText() << ", " << value << ", " << prob << ")";
        else ss << "SymI(" << symbol->idText() << ")";
        return ss.str();
    }
    
    inline void instanceCopy(SymbolInstance *const o) const {_unused(o);};
private:
    void init();
    
    //Reference counting stubs
    void symbolSet();
    void symbolUnset();
};

#endif /* SYMBOLINSTANCE_H */

