#ifndef SYMBOLVECTOR_H
#define SYMBOLVECTOR_H
#include "include.h"
#include "SymbolPtr.h"
#include "SymbolInstance.h"
#include "lib/InstanceVector.h"

typedef InstanceVector<SymbolInstance, SymbolPtr> SymbolVector;

#endif /* SYMBOLVECTOR_H */

