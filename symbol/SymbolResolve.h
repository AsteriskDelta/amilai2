#ifndef SYMBOLRESOLVE_H
#define SYMBOLRESOLVE_H
#include "include.h"

class Symbol;
extern Symbol* SymbolResolve(const idint& id);

#endif /* SYMBOLRESOLVE_H */

