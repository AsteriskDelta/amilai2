#ifndef SYMBOLNODE_H
#define SYMBOLNODE_H
#include "include.h"
#include "SymbolInstance.h"
#include "SymbolVector.h"
#include "relation/RelationVector.h"
#include <list>

class SymbolNode;

struct SymbolLink {
    SymbolNode *node;
    pfloat prob;
};

class SymbolNode : public SymbolVector {
public:
    SymbolNode();
    SymbolNode(const SymbolNode& o) = delete;
    virtual ~SymbolNode();
    
    std::list<SymbolLink> precursors;
    RelationVector relations;
    
    inline operator SymbolVector&() { return *static_cast<SymbolVector*>(this); };
    inline operator RelationVector&() { return this->relations; };
    
    inline SymbolVector& symv() { return *static_cast<SymbolVector*>(this); };
    inline RelationVector& relv() { return this->relations; };
    
    struct {
        bool streamed : 1;
        int padd : 7;
    } flags;
    
    inline void setStreamed() {flags.streamed = true;};
    inline void clearStreamed() {flags.streamed = false; };
    inline bool isStreamed() const { return flags.streamed; };
    inline void setIndexed() {this->setStreamed(); };
    inline void clearIndexed() { this->clearStreamed(); };
    inline bool isIndexed() const { return this->isStreamed(); };
    
    void relate();
    void relateOrder(unsigned int ord);
    void relateExpand(RelationInstance *const rel);
    
    void debugPrint(unsigned short lvl);
    std::string toString();
private:

};

#endif /* SYMBOLNODE_H */