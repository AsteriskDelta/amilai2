#ifndef SYMBOL_H
#define SYMBOL_H
#include "include.h"

class SymbolPrototype;

class Symbol {
public:
    Symbol();
    Symbol(const std::string& newName, idint newId = IDINT_NONE); 
    Symbol(const Symbol&o) = delete;
    virtual ~Symbol();
    
    idint symID;
    std::string name;
    
    struct {
        bool indexed : 1;
        
        int padd : 7;
    } flags;
    
    inline idint id() const {return symID; };
    std::string idText() const;
    
    //Prototype storage- all that implicate this symbol
    //Note their IDs must remain consistent, as they're referenced by 0-order prediction
    //typedef unsigned int ProtoID;
    //Automap<ProtoID, SymbolPrototype> prototypes;
    //inline SymbolPrototype* proto(const ProtoID& protoID) { return prototypes.get(protoID); };
private:
    unsigned long long refCount;
    unsigned int indexCount;
public:
    inline void incRef() { refCount++; };
    inline void decRef() { refCount--; };
    inline void incIdx() { indexCount++; };
    inline void decIdx() { indexCount--; };
    
    inline bool hasName() const { return name.size() > 0; };
    inline const std::string& getName() const { return name; };
};

#endif /* SYMBOL_H */

