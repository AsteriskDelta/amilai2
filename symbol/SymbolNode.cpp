#include "SymbolNode.h"
#include "stream/Stream.h"

SymbolNode::SymbolNode() : precursors() {
  flags.streamed = false;
}

SymbolNode::~SymbolNode() {
  if(flags.streamed) _stream->remove(this);
}

void SymbolNode::debugPrint(unsigned short lvl) {
   const std::string prefix = std::string(lvl, std::string("\t")[0]);
   std::stringstream ss;
   for(SymbolInstance &ins : entries) ss <<((ins == *entries.begin())? "" : ", ") << ins.toString();
   Console::Out(prefix, "SymbolNode{ ", ss.str(), " }");
}

std::string SymbolNode::toString() {
   std::stringstream ss;
   for(SymbolInstance &ins : entries) ss <<((ins == *entries.begin())? "" : ", ") << ins.toString();
   return "SymbolNode{ " + ss.str() + " }";
}

void SymbolNode::relate() {
    auto nodesList = std::vector<SymbolNode*>(); nodesList.push_back(this);

    /*RelationPtr prospect; RelationSearchMeta *prospectMeta;
    while( bool(prospect = _relations->getProspect(this, prospectMeta)) ) {
        pfloat relScore = prospect.evaluate(nodesList);

        node->relv()->addInstance()
    }*/
}

void SymbolNode::relateOrder(unsigned int ord) {
    
}

void SymbolNode::relateExpand(RelationInstance* const rel) {
    
}