#include "Symbol.h"
#include "SymbolManager.h"
#include <sstream>

static inline idint getNextSymID() {
    return _symbols->getNewID();
}

Symbol::Symbol() : symID(getNextSymID()), name(), refCount(0) {
}

Symbol::Symbol(const std::string& n, idint nid) : name(n), refCount(0) {
    if(nid == IDINT_NONE) symID = getNextSymID();
    else symID = nid;
}

Symbol::~Symbol() {
}

std::string Symbol::idText() const {
    std::stringstream ss;
    idint printSymID = this->id();
    const bool isGlobalSym = printSymID > (idint(1)<<61);
    if(isGlobalSym) {
        printSymID = (idint(1)<<62) - printSymID;
        ss << "g_";
    }
    ss << printSymID;
    return ss.str();
}