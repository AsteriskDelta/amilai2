#ifndef SYMBOLMANAGER_H
#define SYMBOLMANAGER_H
#include "include.h"
#include "SymbolPtr.h"
#include "Symbol.h"
#include <lib/InstanceManager.h>

struct SymbolLoadData {
    idint id;
};

class SymbolManager : public InstanceManager<idint, SymbolPtr, SymbolLoadData> {
public:
    typedef InstanceManager<idint, SymbolPtr, SymbolLoadData> InstanceManagerParent;
    
    SymbolManager(bool isGlobal = false);
    SymbolManager(const SymbolManager& orig) = delete;
    virtual ~SymbolManager();
    
    SymbolPtr load(idint id);
    void unload(idint id);
protected:
    
public:
    
};

#endif /* SYMBOLMANAGER_H */

