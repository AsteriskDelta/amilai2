#include "SymbolManager.h"
#include "lib/InstanceManager.cpp"
#include "lib/Registrar.h"
//#include <algorithm>

__thread SymbolManager *_symbols = nullptr;
SymbolManager *_globalSymbols;
template class InstanceManager<idint, SymbolPtr, SymbolLoadData>;

static void AllocSymbolManager() {
    _globalSymbols = new SymbolManager(true);
    Console::Out("Global SymbolManager allocated at ", _globalSymbols);
}
_preregister(AllocSymbolManager);

SymbolManager::SymbolManager(bool isGlobal) : InstanceManagerParent(isGlobal) {
}

SymbolManager::~SymbolManager() {
}

SymbolPtr SymbolManager::load(idint id) {
    _unused(id);
    return SymbolPtr(nullptr);
}
void SymbolManager::unload(idint id) {
    _unused(id);
}