default:debug

TARGET_PLATFORM ?= Desktop
ifeq ($(TARGET_PLATFORM),)
OBJ_DIR ?= build/
else
OBJ_DIR ?= build/$(TARGET_PLATFORM)/
endif
OUT_DIR = $(OBJ_DIR)

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
ABS_DIR := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))
CUR_DIR ?= $(CURDIR)

NAME = AmilAI2
CXX_SYS_FLAGS =
TARG_DIR = out/

#PS_NAME = ScriptSocket
#SS_NAME = ScriptServer

CC_SYS_FLAGS =
LIB_DIR = lib

BUILD_DIRS = lib symbol relation stream intrinsic drive test/* testcase ai
BUILD_DYN = 

DYN_LIB_FLAGS = 
#DYN_LIB_ARGS = $(patsubst dyn/lib%.so, -l% , $(BUILD_DYN))
DYN_LIB_ARGS = 
LIB_CLASSES = 

PCH =
OBJS += $(foreach srcDir, $(BUILD_DIRS), $(patsubst $(srcDir)%.cpp,$(OBJ_DIR)/$(srcDir)%.o,$(shell find $(srcDir)/ -type f -name '*.cpp')))
EXECS += $(patsubst %.cpp,%,$(wildcard *.cpp)) $(patsubst test/%.cpp,test/%,$(wildcard test/*.cpp))
#EX_LIB_ScriptConsole = 
#LIB_OBJS += $(patsubst lib/%.cpp,objects/lib/%.o,$(shell find lib/ -type f -name '*.cpp'))

ORIG_FS = $(FS)
ifeq ($(FS),)
FS = /
#@echo "FS env variable is not defined, defaulting to '/'!"
endif
ifeq ($(FSDEV),)
FSDEV = /
#@echo "FSDEV env variable is not defined, defaulting to '/'!"
endif

#point to slash on non-device builds
debug release install: FS ?= /
debug release install: FSDEV ?= /

CXX ?= g++
OLDCXX ?= g++
debug: OPTI ?= -O3 -Ofast
release: OPTI ?= -O3 -Ofast
debug: CXXPLUS += $(OPTI) -g -DDEBUG
release: CXXPLUS += $(OPTI)  -DOPTIMIZE

ARKE_PATH = ./engine/
ARKE_LIBS = -Wl,-Bstatic -lboost_system -lboost_filesystem -Wl,-Bdynamic -lGL -lGLEW -lGLU `sdl2-config --libs` -L./dyn/ -lassimp -lfreetype -lnoise -lpthread  -ltbb -lsnappy 


ARKE_CFLAGS = `sdl2-config --cflags` `freetype-config --cflags`
#ARKE_INC =-I$(ARKE_PATH)lib/ -I$(ARKE_PATH)shared/ -I$(ARKE_PATH)client/ -I$(ARKE_PATH)  -I$(ARKE_PATH)/ext/ -I$(GS_DIR) -Iext/ann/include/
ARKE_INC = 
LIBS += -lLARKE $(ARKE_LIBS)
#LIBS += -L$(ARKE_PATH) -lEngine $(ARKE_LIBS)

CXXPLUS +=  -MMD -MP
CXXPLUS += $(ARKE_CFLAGS)
CXXPLUS +=  -I$(LIB_DIR) -I$(CUR_DIR) -I/usr/include/ $(ARKE_INC)
CFLAGS = -std=c++1y -fopenmp -fms-extensions $(CXX_SYS_FLAGS) -Wl,--no-as-needed -pipe -c $(CXXPLUS) -Wno-unused-result
BFLAGS = -pipe -c $(CXXPLUS)
CFLAGSNW = $(CFLAGS)
CFLAGS += -Wall -Wextra -Wnon-virtual-dtor -Wcast-align -Wshadow#   -Woverloaded-virtual
LFLAGS = -std=c++1y -fopenmp -fms-extensions $(CXX_SYS_FLAGS) -Wall -Wextra -pipe -I./ $(CXXPLUS) 
LFLAGS += -Wl,-rpath $(ARKE_PATH)
#release: LFLAGS += -Lext/static-linux

LIBS += #-lncurses

CLIENT_CXXF =

INCPATH = 
devInstall device: INCPATH += -I$(FS)usr/include/
debug:   LFLAGS +=
release: LFLAGS +=
dyn:	CFLAGS += -shared -fPIC

CFLAGS += $(INCPATH)

debug : $(PCH) $(BUILD_DYN) $(EXECS) 
release : $(PCH) $(BUILD_DYN) $(EXECS)
dyn : $(LIB_OUT) $(PIC_OUT)
build:debug

#dependent builds
.cpp.o: $(PCH)
	@mkdir -p "$(@D)"
	$(CXX) $(CFLAGS) $< -o $@

$(OBJ_DIR)/%.o: %.cpp %.h
	@mkdir -p "$(@D)"
	$(CXX) $(CFLAGS) $(PIC) $< -o $@

$(EXECS): %: $(OBJS) %.cpp $(API_OUT)
	@mkdir -p $(OUT_DIR)
	@rm $(OUT_DIR)$@ 2> /dev/null || true
	@rm $@ 2> /dev/null || true
	$(CXX) $(INCPATH) $(LFLAGS) -o $(OUT_DIR)$@ -L./ -DPRGM_NAME="$@" $@.cpp $(EX_LIB_$(@)) $(OBJS)  $(LIBS) -pthread 
	@ln -s $(realpath --relative-to=$(dirname $@) $(OUT_DIR)$@) $@

#secondary instructions
clean:
	@find $(OBJ_DIR) -name "*.o" -type f -delete || true
	@find $(OUT_DIR) -name "*.o" -type f -delete || true
	-@rm $(EXECS) 2>/dev/null   || true
	@rm .fuse_hidden* > /dev/null 2>&1 || true

install: $(EXECS)
	@echo "Install to $(FS)"

uninstall:
	

autorun:
	
	
disable:
	

strip: $(EXECS)
	strip -s -R .comment -R .gnu.version --strip-unneeded $(EXECS)
	
scan : clean
	scan-build make 2>&1
	
docs:
	doxygen

docs-install: docs
	

-include $(OBJFILES:.o=.d)
.FORCE:
