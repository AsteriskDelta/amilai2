#ifndef SYMBOLMANAGER_H
#define SYMBOLMANAGER_H
#include "include.h"
#include "SymbolPtr.h"
#include "Symbol.h"
#include "lib/UniqueList.h"
#include <unordered_map>
#include <queue>

struct SymbolLoadData {
    idint id;
};

class SymbolManager;
UniqueList_Pre(SymbolManager, SymbolManager*, doAddParent, doRemoveParent);

class SymbolManager {
public:
    SymbolManager(bool isGlobal = false);
    SymbolManager(const SymbolManager& orig) = delete;
    virtual ~SymbolManager();
    
    void addSymbol(SymbolPtr sym);
    void removeSymbol(idint id);
    
    SymbolPtr getSymbol(idint id);//Always returns a resolved sym
    SymbolPtr getSymbol(const std::string& n);
    
    
    void preloadSymbol(idint id);
    
    idint getNewSymbolID();
    
    void exportAll(SymbolManager *other);
    void unexportAll(SymbolManager *other);
    void exportSymbol(SymbolManager *other, idint symID);
    void unexportSymbol(SymbolManager *other, idint symID);
    
    bool doAddParent(SymbolManager *parent);
    void doRemoveParent(SymbolManager *parent);
protected:
    void useGlobalScheme();//Call ONLY on global sym manager;
    
    
    SymbolPtr loadSymbol(idint id);
    void unloadSymbol(idint id);
    std::queue<SymbolLoadData> loadQueue;//Contains only resolved syms
    
    std::unordered_map<idint, SymbolPtr> loadedSymbols;
    std::unordered_map<std::string, idint> namedSymbolRef;
    
    idint symbolIDAutoincrement;
    bool isGlobalManager;
public:
    UniqueListCB(SymbolManager*, SymbolManager, doAddParent, doRemoveParent) parents;
    UniqueList<SymbolManager*, SymbolManager> children;
    
    inline std::unordered_map<idint, SymbolPtr>::iterator begin() { return loadedSymbols.begin(); };
    inline std::unordered_map<idint, SymbolPtr>::iterator end() { return loadedSymbols.end(); };
};

UniqueList_Expose(SymbolManager, SymbolManager*, doAddParent, doRemoveParent);

#endif /* SYMBOLMANAGER_H */

