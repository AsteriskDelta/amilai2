#include "SymbolManager.h"
#include "lib/Registrar.h"
#include <algorithm>

SymbolManager *_symbols = nullptr, *_globalSymbols;

static void AllocSymbolManager() {
    _globalSymbols = new SymbolManager(true);
    Console::Out("Global SymbolManager allocated at ", _globalSymbols);
}
_preregister(AllocSymbolManager);

SymbolManager::SymbolManager(bool isGlobal) : loadQueue(), loadedSymbols(), symbolIDAutoincrement(0), 
        isGlobalManager(isGlobal), parents(this), children(this) {
    if(isGlobal) this->useGlobalScheme();
    
}

SymbolManager::~SymbolManager() {
}

void SymbolManager::addSymbol(SymbolPtr sym) {
    if(this->getSymbol(sym.id())) return;//Already indexed
    
    sym->incIdx();//Increment index count
    loadedSymbols[sym.id()] = sym;
    if(sym->hasName()) namedSymbolRef[sym->getName()] = sym.id();
    
    for(SymbolManager* child : children) this->exportSymbol(child, sym.id());
}
SymbolPtr SymbolManager::getSymbol(idint id) {
    auto it = loadedSymbols.find(id); SymbolPtr ret;
    if(it == loadedSymbols.end()) {
        ret = this->loadSymbol(id);
    } else ret = it->second;
    
    return ret;
}

SymbolPtr SymbolManager::getSymbol(const std::string& n) {
    auto it = namedSymbolRef.find(n);
    if(it == namedSymbolRef.end()) return SymbolPtr(nullptr);
    else return this->getSymbol(it->second);
}

void SymbolManager::removeSymbol(idint id) {
    auto it = loadedSymbols.find(id);
    if(it == loadedSymbols.end()) return;
    
    for(SymbolManager* child : children) this->unexportSymbol(child, id);
    
    SymbolPtr sym = it->second;
    if(sym->hasName()) namedSymbolRef.erase(namedSymbolRef.find(sym->getName()));
    
    loadedSymbols.erase(it);
    sym->decIdx();//Lessen index count
}

void SymbolManager::preloadSymbol(idint id) {
    struct SymbolLoadData toAdd;
    toAdd.id = id;
    loadQueue.push(toAdd);
}

idint SymbolManager::getNewSymbolID() {
    if(isGlobalManager) return symbolIDAutoincrement--;
    else return symbolIDAutoincrement++;
}

SymbolPtr SymbolManager::loadSymbol(idint id) {
    _unused(id);
    return SymbolPtr(nullptr);
}
void SymbolManager::unloadSymbol(idint id) {
    _unused(id);
}
    
void SymbolManager::exportAll(SymbolManager *other) {
    for(auto it = this->begin(); it != this->end(); ++it) {
        this->exportSymbol(other, it->first);
    }
}
void SymbolManager::unexportAll(SymbolManager *other) {
    for(auto it = this->begin(); it != this->end(); ++it) {
        this->unexportSymbol(other, it->first);
    }
}
void SymbolManager::exportSymbol(SymbolManager *other, idint symID) {
    other->addSymbol(this->getSymbol(symID));
}
void SymbolManager::unexportSymbol(SymbolManager *other, idint symID) {
    other->removeSymbol(symID);
}

bool SymbolManager::doAddParent(SymbolManager *parent) {
    parent->children.add(this);
    parent->exportAll(this);
    return true;
}
void SymbolManager::doRemoveParent(SymbolManager *parent) {
    parent->children.remove(this);
    parent->unexportAll(this);
}

void SymbolManager::useGlobalScheme() {
    isGlobalManager = true;
    symbolIDAutoincrement = (static_cast<idint>(1) << (SymbolPtr::IDBitLength()-1) ) - 1;
}