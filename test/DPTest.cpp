#include <iostream>
#include <vector>
#include "lib/DeferredPtr.h"

typedef unsigned long long TestID;
const TestID TestNull = 0xFFFFFFFFFFFFFFFF;

struct TestStruct {
  TestID rawID;
  std::string blah;
  
  TestID id() const { return rawID; };
};
std::vector<TestStruct> data;

TestStruct* ResolveStruct(const TestID& tid) {
  if(tid >= data.size()) return nullptr;
  std::cout << "\tresolve " << tid<< ", ret " << &data[tid] << "\n";
  //Expensive disk/network IO operation
  return &data[tid];
}

typedef DeferredPtr<TestStruct, TestID, ResolveStruct, TestNull> TestPtr;

int main() {
  for(int i = 0; i < 100; i++) data.push_back(TestStruct{TestID(i), "itworks"+std::to_string(i)});

  TestPtr tests[7];
  tests[0] = TestID(50);
  tests[2] = &data[89];
  tests[3] = TestID(105);
  tests[4] = &data[50];
  tests[5] = nullptr;
  tests[6] = TestNull;
  
  for(unsigned int i = 0; i < sizeof(tests)/sizeof(TestPtr); i++) {
    TestPtr * ptr = &tests[i];
    std::cout << "#"<<i<<"\n";
    std::cout <<"\tBool cast: " << ((*ptr)? "true" : "false") << "\n";
    std::cout <<"\tID: " << ptr->id()<< "\n";
    std::cout << "\tPtr: " << ptr->ptr()<< "\n";
    std::cout << "\tPossible: " << ptr->possible() << "\n";
    
    if(*ptr) {
      std::cout << "\tVALID, id=" << (*ptr)->id() << ", test=" << (*ptr)->blah << "\n";
    }
    std::cout <<"\tFID: " << ptr->id()<< "\n";
  }
  
  std::cout << "F t0 == t2\t\t" << (tests[0] == tests[2]) << "\n";
  std::cout << "T t0 == t4\t\t" << (tests[0] == tests[4]) << "\n";
  std::cout << "T t0 != t2\t\t" << (tests[0] != tests[2]) << "\n";
  std::cout << "T 105 == t3\t\t" << (tests[3] == TestID(105)) << "\n";
  std::cout << "F 89 == t3\t\t" << (tests[3] == TestID(89)) << "\n";
  std::cout << "T d50 == t0\t\t" << (tests[0] == &data[50] ) << "\n";
  std::cout << "F d49 == t0\t\t" << (tests[0] == &data[49] ) << "\n";
  std::cout << "T t0\t\t\t" << bool(tests[0]) << "\n";
  std::cout << "F t5\t\t\t" << bool(tests[5]) << "\n";
  std::cout << "F t6\t\t\t" << bool(tests[6]) << "\n";
  std::cout << "F t1\t\t\t" << bool(tests[1]) << "\n";
  
  return 0;
}
